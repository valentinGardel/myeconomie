package fr.minecraft.MyEconomie.Controllers;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import fr.minecraft.MyEconomie.Models.Currency;
import fr.minecraft.MyEconomie.Models.ShopSign;

public class AdminShopController
{
	/**
	 * Map of the selected shop by players
	 */
	private static HashMap<Player, Location> signSelected = new HashMap<Player, Location>();

	/**
	 * When a player select a sign
	 * 
	 * @param player who select
	 * @param sign   selected
	 */
	public static void SelectSign(Player player, Location location)
	{
		if (player != null && location != null)
		{
			signSelected.put(player, location);
			player.sendMessage(ChatColor.GOLD + "Utilisez la commande: \"/adminshop\" pour edit le shop!");
		}
	}

	/**
	 * Get location of the custom shop selected by the player
	 * 
	 * @param player which has selected a shop
	 */
	public static Location Get(Player player)
	{
		return signSelected.get(player);
	}

	public static void UpdateBuy(Player player, double buy)
	{
		Location location = Get(player);
		if (location != null)
		{
			ShopSign shop = GetShop(player);
			if (shop != null)
			{
				shop.buy = buy;
				switch (shop.save())
				{
				case Success:
					player.sendMessage(ChatColor.GREEN + "Prix modifié!");
					shop.updateSign();
					break;
				default:
					player.sendMessage(ChatColor.RED + "Failed");
					break;
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
		} else
			player.sendMessage(ChatColor.RED + "Aucun admin shop selectionné!");
	}

	public static void UpdateSell(Player player, double sell)
	{
		Location location = Get(player);
		if (location != null)
		{
			ShopSign shop = GetShop(player);
			if (shop != null)
			{
				shop.sell = sell;
				switch (shop.save())
				{
				case Success:
					player.sendMessage(ChatColor.GREEN + "Prix modifié!");
					shop.updateSign();
					break;
				default:
					player.sendMessage(ChatColor.RED + "Failed");
					break;
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
		} else
			player.sendMessage(ChatColor.RED + "Aucun admin shop selectionné!");
	}

	public static void UpdateCurrency(Player player, Currency currency)
	{
		Location location = Get(player);
		if (location != null)
		{
			ShopSign shop = GetShop(player);
			if (shop != null)
			{
				shop.currency = currency;
				switch (shop.save())
				{
				case Success:
					player.sendMessage(ChatColor.GREEN + "Monnaie modifié!");
					shop.updateSign();
					break;
				default:
					player.sendMessage(ChatColor.RED + "Failed");
					break;
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
		} else
			player.sendMessage(ChatColor.RED + "Aucun custom shop selectionné!");
	}

	public static ShopSign GetShop(Player player)
	{
		if (Get(player) != null)
			return ShopSign.GetByLocation(Get(player));
		else
			return null;
	}
}
