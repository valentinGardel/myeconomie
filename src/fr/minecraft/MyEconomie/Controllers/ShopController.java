package fr.minecraft.MyEconomie.Controllers;

import java.util.HashMap;

//import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.SignChangeEvent;

import fr.minecraft.MyEconomie.Models.Currency;
import fr.minecraft.MyEconomie.Models.ShopSign;

/**
 * Controller for Shop
 */
public class ShopController
{
	public static final Material[] UNSELLABLE_ITEM =
	{ Material.ENCHANTED_BOOK, Material.SHEARS, Material.TRIDENT, Material.ELYTRA, Material.BOW };

	public static final String[] UNSELLABLE_STRING =
	{ "_SWORD", "_AXE", "_PICKAXE", "_SHOVEL", "_HOE", "_HELMET", "_CHESTPLATE", "_LEGGINGS", "_BOOTS", "SHULKER_BOX" };

	public static double getMoneyValue(String value)
	{
		return Math.round(Double.parseDouble(value) * 100.0) / 100.0;
	}

	/**
	 * Map of the selected shop by players
	 */
	private static HashMap<Player, Location> signSelected = new HashMap<Player, Location>();

	/**
	 * When a player select a sign
	 * 
	 * @param player who select
	 * @param sign   selected
	 */
	public static void SelectSign(Player player, Location location)
	{
		if (player != null && location != null)
		{
			signSelected.put(player, location);
			player.sendMessage(ChatColor.GOLD
					+ "Utilisez la commande: \"/shop [take|store|update] ...\" pour utiliser votre shop!");
		}
	}

	public static void Take(Player player, int amount)
	{
		Location location = signSelected.get(player);
		if (location != null)
		{
			ShopSign sign = ShopSign.GetByLocation(location);
			if (sign != null)
			{
				switch (sign.take(player, amount))
				{
				case Success:
					break;
				case NotEnoughItem:
					player.sendMessage(ChatColor.RED + "Le shop n'a pas autant d'item disponible!");
					break;
				default:
					player.sendMessage(ChatColor.RED + "Error!");
					break;
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop selectionné est introuvable!");
		} else
			player.sendMessage(ChatColor.RED
					+ "Aucun shop selectionné, veuillez faire un clic droit sur votre panneau de shop pour le selectionner!");
	}

	public static void Store(Player player, int amount)
	{
		Location location = signSelected.get(player);
		if (location != null)
		{
			ShopSign sign = ShopSign.GetByLocation(location);
			if (sign != null)
			{
				switch (sign.store(player, amount))
				{
				case Success:
					break;
				case NotEnoughItem:
					player.sendMessage(ChatColor.RED + "Vous n'avez pas assez d'item disponible!");
					break;
				default:
					player.sendMessage(ChatColor.RED + "Error!");
					break;
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop selectionné est introuvable!");
		} else
			player.sendMessage(ChatColor.RED
					+ "Aucun shop selectionné, veuillez faire un clic droit sur votre panneau de shop pour le selectionner!");
	}

	public static Location Get(Player player)
	{
		return signSelected.get(player);
	}

	public static void ProcessShopSignCreation(SignChangeEvent event, boolean isAdminShop)
	{
		String[] lines = event.getLines();
		if (lines.length == 4)
		{
			Material material = null;
			try
			{
				if (lines[1].isEmpty())
					material = event.getPlayer().getInventory().getItemInMainHand().getType();
				else
					material = Material.valueOf(lines[1].toString().toUpperCase());
			} catch (IllegalArgumentException e)
			{
			}
			if (material != null)
			{
				if (isSellable(material))
				{
					ShopController.ProcessThirdLineSignCreation(event, lines, material, isAdminShop);
				} else
				{
					event.getPlayer().sendMessage(ChatColor.RED + "Impossible de mettre en vente cet item!");
					event.setCancelled(true);
				}
			} else
			{
				event.getPlayer().sendMessage(
						ChatColor.RED + "Le nom de l'item est introuvable, réferencez-vous à la documentation!");
				event.setCancelled(true);
			}
		} else
		{
			event.getPlayer().sendMessage(
					ChatColor.RED + "Le nombre de lignes n'est pas valide, réferencez-vous à la documentation!");
			event.setCancelled(true);
		}
	}

	public static void ProcessThirdLineSignCreation(SignChangeEvent event, String[] lines, Material material,
			boolean isAdminShop)
	{
		String[] split = lines[2].replace(" ", "").split(":");
		String currencySymbol = null;
		double buy = -1, sell = -1;

		try
		{
			switch (split.length)
			{
			case 1:
				if (split[0].startsWith("b"))
				{
					currencySymbol = split[0].replaceFirst("b", "").replaceAll("[.0123456789]", "");
					String split1 = split[0].replaceFirst("b", "").replace(currencySymbol, "");
					buy = ShopController.getMoneyValue(split1);
					if (buy < 0 || buy == -1)
					{
						event.getPlayer()
								.sendMessage(ChatColor.RED + "Prix invalide, réferencez-vous à la documentation!");
						event.setCancelled(true);
						return;
					}
				} else if (split[0].startsWith("s"))
				{
					currencySymbol = split[0].replaceFirst("s", "").replaceAll("[.0123456789]", "");
					String split1 = split[0].replaceFirst("s", "").replace(currencySymbol, "");
					sell = ShopController.getMoneyValue(split1);
					if (sell < 0 || sell == -1)
					{
						event.getPlayer()
								.sendMessage(ChatColor.RED + "Prix invalide, réferencez-vous à la documentation!");
						event.setCancelled(true);
						return;
					}
				} else
				{
					event.getPlayer()
							.sendMessage(ChatColor.RED + "Ligne 3 invalide, réferencez-vous à la documentation!");
					event.setCancelled(true);
					return;
				}
				break;
			case 2:
				String currencySymbolBuy = null, currencySymbolSell = null;
				if (split[0].startsWith("b") && split[1].startsWith("s"))
				{
					currencySymbolBuy = split[0].replaceFirst("b", "").replaceAll("[.0123456789]", "");
					String split1 = split[0].replaceFirst("b", "").replace(currencySymbolBuy, "");
					buy = ShopController.getMoneyValue(split1);

					currencySymbolSell = split[1].replaceFirst("s", "").replaceAll("[.0123456789]", "");
					String split2 = split[1].replaceFirst("s", "").replace(currencySymbolSell, "");
					sell = ShopController.getMoneyValue(split2);
				} else if (split[1].startsWith("b") && split[0].startsWith("s"))
				{
					currencySymbolSell = split[0].replaceFirst("s", "").replaceAll("[.0123456789]", "");
					String split1 = split[0].replaceFirst("s", "").replace(currencySymbolSell, "");
					sell = ShopController.getMoneyValue(split1);

					currencySymbolBuy = split[1].replaceFirst("b", "").replaceAll("[.0123456789]", "");
					String split2 = split[1].replaceFirst("b", "").replace(currencySymbolBuy, "");
					buy = ShopController.getMoneyValue(split2);
				} else
				{
					event.getPlayer()
							.sendMessage(ChatColor.RED + "Ligne 3 invalide, réferencez-vous à la documentation!");
					event.setCancelled(true);
					return;
				}

				if (buy < 0 || buy == -1 || sell < 0 || sell == -1)
				{
					event.getPlayer().sendMessage(ChatColor.RED + "Prix invalide, réferencez-vous à la documentation!");
					event.setCancelled(true);
					return;
				}
				if (currencySymbolBuy == null || currencySymbolBuy.isEmpty() || currencySymbolSell == null
						|| currencySymbolSell.isEmpty())
				{
					currencySymbol = (currencySymbolBuy == null || currencySymbolBuy.isEmpty()) ? currencySymbolSell
							: currencySymbolBuy;
				} else if (currencySymbolBuy.equalsIgnoreCase(currencySymbolSell))
				{
					currencySymbol = currencySymbolBuy;
				} else
				{
					event.getPlayer().sendMessage(
							ChatColor.RED + "La monnaie utilisé pour l'achat et la vente doivent étre la méme!");
					event.setCancelled(true);
					return;
				}
				break;
			default:
				event.getPlayer().sendMessage(ChatColor.RED + "Ligne 3 invalide, réferencez-vous à la documentation!");
				event.setCancelled(true);
				return;
			}

//			Bukkit.broadcastMessage("CurrencySymbol: "+currencySymbol);
//			Bukkit.broadcastMessage("Buy: "+buy);
//			Bukkit.broadcastMessage("Sell: "+sell);
//			Bukkit.broadcastMessage("processing line 4 ... ");

			ProcessFourthLineSignCreation(event, lines, currencySymbol, buy, sell, material, isAdminShop);
		} catch (NumberFormatException e)
		{
			event.getPlayer()
					.sendMessage(ChatColor.RED + "Chiffre en ligne 3 invalide, réferencez-vous à la documentation!");
			event.setCancelled(true);
		}
	}

	public static void ProcessFourthLineSignCreation(SignChangeEvent event, String[] lines, String currencySymbol,
			double buy, double sell, Material material, boolean isAdminShop)
	{
		try
		{
			Currency currency = null;
			if (currencySymbol == null || currencySymbol.isEmpty())
				currency = Currency.GetDefault();
			else
				currency = Currency.GetBySymbol(currencySymbol);

			if (currency != null)
			{
				if (currency.tradable)
				{
					int maxQuantity = (isAdminShop) ? 0 : Integer.parseInt(lines[3]);

					if (maxQuantity >= 0)
					{
						switch (new ShopSign(event.getBlock().getLocation(), (isAdminShop) ? null : event.getPlayer(),
								buy, sell, material, 0, maxQuantity, currency).save())
						{
						case Success:
							event.getPlayer().sendMessage(ChatColor.GREEN + "Panneau de magasin créé!");
							event.setLine(0, (isAdminShop) ? ShopSign.FIRST_LINE_ADMIN : ShopSign.FIRST_LINE_USER);
							String materialName = material.name();
							// if name longer then max line length
							if (materialName.length() > 15)
								materialName = materialName.substring(0, 14);
							event.setLine(1, materialName);
							event.setLine(2, ShopSign.FormatBuySellLine(buy, sell, currency));

							if (isAdminShop)
								event.setLine(3, "");
							else
								event.setLine(3, "0/" + maxQuantity);
							break;
						default:
							event.getPlayer().sendMessage(ChatColor.RED + "Erreur!");
							event.setCancelled(true);
							break;
						}
					} else
					{
						event.getPlayer()
								.sendMessage(ChatColor.RED + "La quantité maximum ne peut pas étre inférieur à 0!");
						event.setCancelled(true);
					}
				} else
				{
					event.getPlayer().sendMessage(ChatColor.RED + "Cette monnaie n'est pas échangeable!");
					event.setCancelled(true);
				}
			} else
			{
				event.getPlayer().sendMessage(ChatColor.RED + "Monnaie invalide, réferencez-vous à la documentation!");
				event.setCancelled(true);
			}

		} catch (NumberFormatException e)
		{
			event.getPlayer()
					.sendMessage(ChatColor.RED + "Chiffre en ligne 4 invalide, réferencez-vous à la documentation!");
			event.setCancelled(true);
		}
	}

	public static boolean isSellable(Material material)
	{
		String name = material.name();
		for (String n : UNSELLABLE_STRING)
		{
			if (name.endsWith(n))
				return false;
		}
		for (Material m : UNSELLABLE_ITEM)
		{
			if (material.equals(m))
				return false;
		}
		return true;
	}
}
