package fr.minecraft.MyEconomie.Controllers;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.block.SignChangeEvent;

import fr.minecraft.MyEconomie.Models.Currency;
import fr.minecraft.MyEconomie.Models.CustomShop;
import fr.minecraft.MyEconomie.Models.Wallet;

public class CustomShopController
{
	/**
	 * Map of the selected shop by players
	 */
	private static HashMap<Player, Location> signSelected = new HashMap<Player, Location>();

	/**
	 * When a player select a sign
	 * 
	 * @param player who select
	 * @param sign   selected
	 */
	public static void SelectSign(Player player, Location location)
	{
		if (player != null && location != null)
		{
			signSelected.put(player, location);
			player.sendMessage(ChatColor.GOLD + "Utilisez la commande: \"/customshop\" pour edit le shop!");
		}
	}

	public static void Buy(Player player, Location location)
	{
		if (location != null)
		{
			CustomShop shop = CustomShop.GetByLocation(location);
			if (shop != null)
			{
				Wallet wallet = Wallet.GetByPlayer(player);
				if (wallet.canPay(shop.currency, shop.buy))
				{
					boolean canBuy = true;
					// verif require
					for (String permission : shop.requires)
					{
						if (!player.hasPermission(permission))
						{
							canBuy = false;
							break;
						}
					}
					// verif forbidden
					if (canBuy)
					{
						for (String permission : shop.forbiddens)
						{
							if (player.hasPermission(permission))
							{
								canBuy = false;
								break;
							}
						}
						if (canBuy)
						{
							// take money
							wallet.removeCurrency(shop.currency, shop.buy);
							// exec commands
							ConsoleCommandSender console = Bukkit.getConsoleSender();
							for (String command : shop.commands)
							{
								Bukkit.dispatchCommand(console, TranslateCommand(command, player));
							}
							player.sendMessage(
									ChatColor.GREEN + "Vous avez été débité de " + shop.buy + shop.currency.symbol);
							shop.hasBeenBuyed();
						} else
							player.sendMessage(ChatColor.RED + "Vous ne pouvez pas acheter ca!");
					} else
						player.sendMessage(
								ChatColor.RED + "Vous ne pouvez pas acheter ca, il vous manque quelque chose!");
				} else
					player.sendMessage(ChatColor.RED + "Vous n'avez pas assez de fonds pour acheter ca!");
			} else
				player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
		} else
			player.sendMessage(ChatColor.RED + "Aucun custom shop selectionné!");
	}

	/**
	 * Replace tags of custom shop command by the proper values
	 * 
	 * @return String of the commande translated
	 */
	private static String TranslateCommand(String command, Player player)
	{
		command = command.replaceAll("@player", player.getName());
		return command;
	}

	/**
	 * Get location of the custom shop selected by the player
	 * 
	 * @param player which has selected a shop
	 */
	public static Location Get(Player player)
	{
		return signSelected.get(player);
	}

	public static void AddRequire(Player player, String require)
	{
		Location location = Get(player);
		if (location != null)
		{
			CustomShop shop = CustomShop.GetByLocation(location);
			if (shop != null)
			{
				switch (shop.addRequire(require))
				{
				case Success:
					player.sendMessage(ChatColor.GREEN + "Require ajouté!");
					break;
				default:
					player.sendMessage(ChatColor.RED + "Failed");
					break;
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
		} else
			player.sendMessage(ChatColor.RED + "Aucun custom shop selectionné!");
	}

	public static void AddForbidden(Player player, String forbidden)
	{
		Location location = Get(player);
		if (location != null)
		{
			CustomShop shop = CustomShop.GetByLocation(location);
			if (shop != null)
			{
				switch (shop.addForbidden(forbidden))
				{
				case Success:
					player.sendMessage(ChatColor.GREEN + "Forbidden ajouté!");
					break;
				default:
					player.sendMessage(ChatColor.RED + "Failed");
					break;
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
		} else
			player.sendMessage(ChatColor.RED + "Aucun custom shop selectionné!");
	}

	public static void AddCommand(Player player, String command)
	{
		Location location = Get(player);
		if (location != null)
		{
			CustomShop shop = CustomShop.GetByLocation(location);
			if (shop != null)
			{
				switch (shop.addCommand(command))
				{
				case Success:
					player.sendMessage(ChatColor.GREEN + "Commande ajouté!");
					break;
				default:
					player.sendMessage(ChatColor.RED + "Failed");
					break;
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
		} else
			player.sendMessage(ChatColor.RED + "Aucun custom shop selectionné!");
	}

	/**
	 * Display custom shop info to the player
	 * 
	 * @param player to display info
	 */
	public static void Info(Player player)
	{
		Location location = Get(player);
		if (location != null)
		{
			CustomShop shop = CustomShop.GetByLocation(location);
			if (shop != null)
			{
				player.sendMessage(ChatColor.GOLD + "Requires:");
				if (shop.requires.isEmpty())
					player.sendMessage("aucun requires");
				else
				{
					for (String i : shop.requires)
						player.sendMessage(" - " + i);
				}
				player.sendMessage(ChatColor.GOLD + "Forbiddens:");
				if (shop.forbiddens.isEmpty())
					player.sendMessage("aucun forbiddens");
				else
				{
					for (String i : shop.forbiddens)
						player.sendMessage(" - " + i);
				}
				player.sendMessage(ChatColor.GOLD + "Commands:");
				if (shop.commands.isEmpty())
					player.sendMessage("aucune commands");
				else
				{
					for (String i : shop.commands)
						player.sendMessage(" - " + i);
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
		} else
			player.sendMessage(ChatColor.RED + "Aucun custom shop selectionné!");
	}

	public static void RemoveRequire(Player player, String require)
	{
		Location location = Get(player);
		if (location != null)
		{
			CustomShop shop = CustomShop.GetByLocation(location);
			if (shop != null)
			{
				switch (shop.removeRequire(require))
				{
				case Success:
					player.sendMessage(ChatColor.GREEN + "Require supprimé!");
					break;
				default:
					player.sendMessage(ChatColor.RED + "Failed");
					break;
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
		} else
			player.sendMessage(ChatColor.RED + "Aucun custom shop selectionné!");
	}

	public static void RemoveForbidden(Player player, String forbidden)
	{
		Location location = Get(player);
		if (location != null)
		{
			CustomShop shop = CustomShop.GetByLocation(location);
			if (shop != null)
			{
				switch (shop.removeForbidden(forbidden))
				{
				case Success:
					player.sendMessage(ChatColor.GREEN + "Forbidden supprimé!");
					break;
				default:
					player.sendMessage(ChatColor.RED + "Failed");
					break;
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
		} else
			player.sendMessage(ChatColor.RED + "Aucun custom shop selectionné!");
	}

	public static void RemoveCommand(Player player, String command)
	{
		Location location = Get(player);
		if (location != null)
		{
			CustomShop shop = CustomShop.GetByLocation(location);
			if (shop != null)
			{
				switch (shop.removeCommand(command))
				{
				case Success:
					player.sendMessage(ChatColor.GREEN + "Command supprimé!");
					break;
				default:
					player.sendMessage(ChatColor.RED + "Failed");
					break;
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
		} else
			player.sendMessage(ChatColor.RED + "Aucun custom shop selectionné!");
	}

	public static void UpdateBuy(Player player, double buy)
	{
		Location location = Get(player);
		if (location != null)
		{
			CustomShop shop = CustomShop.GetByLocation(location);
			if (shop != null)
			{
				shop.buy = buy;
				switch (shop.save())
				{
				case Success:
					player.sendMessage(ChatColor.GREEN + "Prix modifié!");
					shop.updateSign();
					break;
				default:
					player.sendMessage(ChatColor.RED + "Failed");
					break;
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
		} else
			player.sendMessage(ChatColor.RED + "Aucun custom shop selectionné!");
	}

	public static void UpdateDescription(Player player, String description)
	{
		if (description.length() <= CustomShop.LINE_MAX_LENGTH * 2)
		{
			Location location = Get(player);
			if (location != null)
			{
				CustomShop shop = CustomShop.GetByLocation(location);
				if (shop != null)
				{
					shop.description = description;
					switch (shop.save())
					{
					case Success:
						player.sendMessage(ChatColor.GREEN + "Description modifié!");
						shop.updateSign();
						break;
					default:
						player.sendMessage(ChatColor.RED + "Failed");
						break;
					}
				} else
					player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
			} else
				player.sendMessage(ChatColor.RED + "Aucun custom shop selectionné!");
		} else
			player.sendMessage(ChatColor.RED + "Description trop grande! Taile max: " + CustomShop.LINE_MAX_LENGTH * 2
					+ " characteres!");
	}

	public static void UpdateCurrency(Player player, Currency currency)
	{
		Location location = Get(player);
		if (location != null)
		{
			CustomShop shop = CustomShop.GetByLocation(location);
			if (shop != null)
			{
				shop.currency = currency;
				switch (shop.save())
				{
				case Success:
					player.sendMessage(ChatColor.GREEN + "Monnaie modifié!");
					shop.updateSign();
					break;
				default:
					player.sendMessage(ChatColor.RED + "Failed");
					break;
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
		} else
			player.sendMessage(ChatColor.RED + "Aucun custom shop selectionné!");
	}

	public static void UpdateBreakAfterBuyed(Player player, boolean breakAfterBuyed)
	{
		Location location = Get(player);
		if (location != null)
		{
			CustomShop shop = CustomShop.GetByLocation(location);
			if (shop != null)
			{
				shop.breakAfterBuyed = breakAfterBuyed;
				switch (shop.save())
				{
				case Success:
					player.sendMessage(
							ChatColor.GREEN + "BreakAfterBuyed set to " + (breakAfterBuyed ? "true" : "false") + "!");
					shop.updateSign();
					break;
				default:
					player.sendMessage(ChatColor.RED + "Failed");
					break;
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
		} else
			player.sendMessage(ChatColor.RED + "Aucun custom shop selectionné!");
	}

	public static void ProcessShopSignCreation(SignChangeEvent event)
	{
		Player player = event.getPlayer();
		String[] lines = event.getLines();
		if (lines.length == 4)
		{
			String line = lines[1].replace(" ", "");
			if (!line.isEmpty())
			{
				String currencySymbol = null;
				double buy = -1;
				currencySymbol = line.replaceFirst("b", "").replaceAll("[.0123456789]", "");
				Currency currency = null;
				if (currencySymbol.isEmpty())
					currency = Currency.GetDefault();
				else
					currency = Currency.GetByNameOrSymbol(currencySymbol);
				if (currency != null)
				{
					String price = line.replaceFirst("b", "").replace(currencySymbol, "");
					buy = Math.round(Double.parseDouble(price) * 100) / 100;
					if (buy < 0 || buy == -1)
					{
						player.sendMessage(ChatColor.RED + "Prix invalide, réferencez-vous à la documentation!");
						event.setCancelled(true);
					} else
					{
						Location location = event.getBlock().getLocation();
						CustomShop shop = new CustomShop(location, currency, buy);
						switch (shop.save())
						{
						case Success:
							player.sendMessage(ChatColor.GREEN + "Custom Shop créé!");
							SelectSign(player, location);
							event.setLine(0, CustomShop.FIRST_LINE);
							event.setLine(1, buy + currency.symbol);
							break;
						default:
							player.sendMessage(ChatColor.RED + "Failed");
							event.setCancelled(true);
							break;
						}
					}
				} else
					player.sendMessage(ChatColor.RED + "Monnaie invalide!");
			} else
			{
				event.getPlayer().sendMessage(ChatColor.RED + "La ligne 2 est invalide!");
				event.setCancelled(true);
			}
		} else
		{
			player.sendMessage(
					ChatColor.RED + "Le nombre de lignes n'est pas valide, réferencez-vous à la documentation!");
			event.setCancelled(true);
		}
	}

	public static CustomShop GetShop(Player player)
	{
		if (Get(player) != null)
			return CustomShop.GetByLocation(Get(player));
		else
			return null;
	}
}
