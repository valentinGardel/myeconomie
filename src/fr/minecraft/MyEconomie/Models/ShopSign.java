package fr.minecraft.MyEconomie.Models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Sign;
import org.bukkit.block.sign.Side;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import ActiveRecord.Model;
import fr.minecraft.MyEconomie.Config;
import fr.minecraft.MyEconomie.Utils.Code;

/**
 * Class who represent a shop sign
 */
public class ShopSign extends Model
{
	public static final String TABLE_NAME = "ShopSign", FIRST_LINE_USER = "[SHOP]", FIRST_LINE_ADMIN = "[ADMIN]";
	private static final String ADMIN_OWNER = "ADMIN";

	private boolean isInDatabase;
	public Location location;
	public OfflinePlayer owner;
	public double buy, sell;
	public Currency currency;
	public Material item;
	public int itemQuantity, maxQuantity;

	/**
	 * Default constructor used from outside this class to create a sign
	 * 
	 * @param location   of the sign
	 * @param owner      of the sign, null if admin
	 * @param buy        price
	 * @param sell       price
	 * @param item       to buy/sell
	 * @param amountItem in the shop (probably 0 at creation)
	 * @param amountMax  that the shop can buy
	 * @param currency   of the shop
	 */
	public ShopSign(Location location, Player owner, double buy, double sell, Material item, int amountItem,
			int amountMax, Currency currency)
	{
		this.location = location;
		this.owner = owner;
		this.buy = buy;
		this.sell = sell;
		this.item = item;
		this.itemQuantity = amountItem;
		this.maxQuantity = amountMax;
		this.currency = currency;
		this.isInDatabase = false;
	}

	/**
	 * Private constructor used when retrieved from database
	 */
	private ShopSign(String worldUUID, int x, int y, int z, String ownerUUID, double buy, double sell, String item,
			int amountItem, int amountMax, int currencyId)
	{
		this.location = new Location(Bukkit.getWorld(UUID.fromString(worldUUID)), x, y, z);
		if (ownerUUID.equalsIgnoreCase(ADMIN_OWNER))
		{
			this.owner = null;
		} else
		{
			this.owner = Bukkit.getOfflinePlayer(UUID.fromString(ownerUUID));
		}
		this.buy = buy;
		this.sell = sell;
		this.item = Material.valueOf(item);
		this.itemQuantity = amountItem;
		this.maxQuantity = amountMax;
		this.currency = Currency.GetById(currencyId);
		this.isInDatabase = true;
	}

	/**
	 * Creation of database tables if not exists
	 */
	public static void CreateTable()
	{
		try
		{
			Statement stmt = Config.GetConfig().getConnection().createStatement();
			stmt.executeUpdate("create table if not exists " + TABLE_NAME
					+ "(worldUUID varchar(50), x int, y int, z int, buy decimal(19,2), sell decimal(19,2), ownerUUID varchar(50), item varchar(64), amount int, amountMax int, currency int REFERENCES Currency(id) ON DELETE CASCADE, PRIMARY KEY (worldUUID, x, y, z));");
			stmt.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
	}

	/**
	 * Save the shop in the database
	 * 
	 * @return Code Failed or Success
	 */
	public Code save()
	{
		try
		{
			// update in db
			if (this.isInDatabase)
			{
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("update " + TABLE_NAME
						+ " set amount=?, amountMax=? where worldUUID=? and x=? and y=? and z=?;");
				st.setInt(1, this.itemQuantity);
				st.setInt(2, this.maxQuantity);
				st.setString(3, this.location.getWorld().getUID().toString());
				st.setInt(4, this.location.getBlockX());
				st.setInt(5, this.location.getBlockY());
				st.setInt(6, this.location.getBlockZ());
				st.executeUpdate();
				st.close();
			}
			// insert in db
			else
			{
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("insert into " + TABLE_NAME
						+ " (worldUUID, x, y, z, buy, sell, ownerUUID, item, amount, amountMax, currency) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

				st.setString(1, this.location.getWorld().getUID().toString());
				st.setInt(2, this.location.getBlockX());
				st.setInt(3, this.location.getBlockY());
				st.setInt(4, this.location.getBlockZ());
				st.setDouble(5, this.buy);
				st.setDouble(6, this.sell);
				st.setString(7, (this.owner != null) ? this.owner.getUniqueId().toString() : ADMIN_OWNER);
				st.setString(8, this.item.toString());
				st.setInt(9, this.itemQuantity);
				st.setInt(10, this.maxQuantity);
				st.setInt(11, this.currency.getId());
				st.executeUpdate();
				st.close();
			}
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
			return Code.Failed;
		}
		return Code.Success;
	}

	/**
	 * Delete the shop in the database
	 * 
	 * @return Code Failed or Success
	 */
	public Code delete()
	{
		try
		{
			// update in db
			if (this.isInDatabase)
			{
				PreparedStatement st = Config.GetConfig().getConnection()
						.prepareStatement("DELETE FROM " + TABLE_NAME + " WHERE worldUUID=? and x=? and y=? and z=?;");
				st.setString(1, this.location.getWorld().getUID().toString());
				st.setInt(2, this.location.getBlockX());
				st.setInt(3, this.location.getBlockY());
				st.setInt(4, this.location.getBlockZ());
				st.executeUpdate();
				st.close();
				this.isInDatabase = false;
			} else
				return Code.NotInDatabase;
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
			return Code.Failed;
		}
		return Code.Success;
	}

	/**
	 * Retieve the shop at the given location
	 * 
	 * @param location of the shop to retrieve
	 * @return ShopSign
	 */
	public static ShopSign GetByLocation(Location location)
	{
		ShopSign sign = null;
		try
		{
			PreparedStatement st = Config.GetConfig().getConnection().prepareStatement(
					"Select worldUUID, x, y, z, ownerUUID, buy, sell, item, amount, amountMax, currency from "
							+ TABLE_NAME + " where worldUUID=? and x=? and y=? and z=?;");
			st.setString(1, location.getWorld().getUID().toString());
			st.setInt(2, location.getBlockX());
			st.setInt(3, location.getBlockY());
			st.setInt(4, location.getBlockZ());
			ResultSet res = st.executeQuery();
			if (res.next())
			{
				sign = new ShopSign(res.getString("worldUUID"), res.getInt("x"), res.getInt("y"), res.getInt("z"),
						res.getString("ownerUUID"), res.getDouble("buy"), res.getDouble("sell"), res.getString("item"),
						res.getInt("amount"), res.getInt("amountMax"), res.getInt("currency"));
			}
			st.close();
			res.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
		return sign;
	}

	/**
	 * Buy from the shop
	 * 
	 * @param walletFrom is the wallet who pay
	 * @param amount     of item to buy
	 * @param player     who buy
	 * @return Code
	 */
	public Code buy(Wallet walletFrom, int amount, Player player)
	{
		if (this.buy >= 0)
		{
			if (amount > 0)
			{
				if (this.owner != null)
				{
					if (this.itemQuantity >= amount)
					{
						if (walletFrom.canPay(this.currency, amount * this.buy))
						{
							Wallet walletTo = Wallet.GetByPlayer(this.owner);
							if (walletTo != null)
							{
								if (Wallet.Transfer(walletFrom, walletTo, currency, amount * this.buy))
								{
									this.itemQuantity -= amount;
									this.save();
									this.updateSign();
									this.givePlayer(player, amount);
								} else
									return Code.Failed;
							} else
								return Code.Failed;
						} else
							return Code.NotEnoughFound;
					} else
						return Code.NotEnoughItem;
				} else
				{
					walletFrom.removeCurrency(this.currency, amount * this.buy);
					this.givePlayer(player, amount);
				}
			} else
				return Code.ParamInvalid;
		} else
			return Code.NotAvailable;
		return Code.Success;
	}

	/**
	 * Sell to the shop
	 * 
	 * @param walletTo is the wallet who sell
	 * @param amount   of item to sell
	 * @param player   who sell
	 * @return Code
	 */
	public Code sell(Wallet walletTo, int amount, Player player)
	{
		if (this.sell >= 0)
		{
			if (amount > 0)
			{
				if (this.playerHasEnough(player, amount))
				{
					if (this.owner != null)
					{
						Wallet walletFrom = Wallet.GetByPlayer(this.owner);
						if (walletFrom != null)
						{
							if (this.maxQuantity >= this.itemQuantity + amount)
							{
								if (walletFrom.canPay(this.currency, amount * this.sell))
								{
									if (Wallet.Transfer(walletFrom, walletTo, currency, amount * this.sell))
									{
										this.itemQuantity += amount;
										this.save();
										this.updateSign();
										this.takePlayer(player, amount);
									} else
										return Code.Failed;
								} else
									return Code.NotEnoughFound;
							} else
								return Code.MaxAmountReached;
						} else
							return Code.Failed;
					} else
					{
						walletTo.addCurrency(this.currency, amount * this.sell);
						this.takePlayer(player, amount);
					}
				} else
					return Code.NotEnoughItem;
			} else
				return Code.ParamInvalid;
		} else
			return Code.NotAvailable;
		return Code.Success;
	}

	/**
	 * Retrieve item from the shop and give them to a player (use by the owner)
	 * 
	 * @param player whom we give the items
	 * @param amount of item to give
	 * @return Code
	 */
	public Code take(Player player, int amount)
	{
		if (this.itemQuantity >= amount)
		{
			this.itemQuantity -= amount;
			switch (this.save())
			{
			case Success:
				this.givePlayer(player, amount);
				this.updateSign();
				break;
			default:
				return Code.Failed;
			}
		} else
			return Code.NotEnoughItem;
		return Code.Success;
	}

	/**
	 * Store item to the shop from th given player (use by the owner)
	 * 
	 * @param player who store items
	 * @param amount of item to store
	 * @return Code
	 */
	public Code store(Player player, int amount)
	{
		if (this.playerHasEnough(player, amount))
		{
			this.itemQuantity += amount;
			switch (this.save())
			{
			case Success:
				this.takePlayer(player, amount);
				this.updateSign();
				break;
			default:
				return Code.Failed;
			}
		} else
			return Code.NotEnoughItem;
		return Code.Success;
	}

	/**
	 * Place the given amount of item to the given player
	 * 
	 * @param player to give items
	 * @param amount of item to give
	 */
	private void givePlayer(Player player, int amount)
	{
		ItemStack stack = new ItemStack(this.item);
		stack.setAmount(amount);
		player.getInventory().addItem(stack);
		player.updateInventory();
	}

	/**
	 * Update the sign lines with the right prices and quantities
	 */
	public void updateSign()
	{
		Sign sign = (Sign) this.location.getBlock().getState();
		ShopSign.setSignLine(sign, 2, FormatBuySellLine(this.buy, this.sell, this.currency));
		ShopSign.setSignLine(sign, 3, this.isAdminShop() ? "" : (this.itemQuantity + "/" + this.maxQuantity));
		sign.update();
	}

	public boolean isAdminShop()
	{
		return this.owner == null;
	}

	/**
	 * Format of the line buy/sell
	 * 
	 * @param buy      price
	 * @param sell     price
	 * @param currency of prices
	 * @return String representing the buy/sell format
	 */
	public static String FormatBuySellLine(double buy, double sell, Currency currency)
	{
		if (buy != -1 && sell != -1)
		{
			return "b:" + buy + currency.symbol + " s:" + sell + currency.symbol;
		} else if (buy != -1)
		{
			return "b: " + buy + currency.symbol;
		} else
		{
			return "s: " + sell + currency.symbol;
		}
	}

	/**
	 * Take from player inventory the amount given of items
	 * 
	 * @param player to take items from
	 * @param amount of item to take
	 */
	private void takePlayer(Player player, int amount)
	{
		int remainingAmount = amount;
		HashMap<Integer, ? extends ItemStack> items = player.getInventory().all(this.item);
		Set<Integer> keys = items.keySet();
		for (Integer key : keys)
		{
			int stackAmount = items.get(key).getAmount();
			// si le stack contient moins que ce qu'il reste a enlever
			if (stackAmount < remainingAmount)
			{
				remainingAmount -= stackAmount;
				items.get(key).setAmount(0);
			}
			// si le stack contient plus ou autant que ce qu'il reste a enlever
			else
			{
				// remainingAmount-=stackAmount;
				items.get(key).setAmount(stackAmount - remainingAmount);
				break;
			}
		}
		player.updateInventory();
	}

	public static int PlayerHas(Player player, Material material)
	{
		int amount = 0;
		HashMap<Integer, ? extends ItemStack> items = player.getInventory().all(material);
		Set<Integer> keys = items.keySet();
		for (Integer key : keys)
		{
			amount += items.get(key).getAmount();
		}
		return amount;
	}

	/**
	 * Check if player has enought items
	 * 
	 * @param player to check
	 * @param amount to check
	 * @return if the player has enough item
	 */
	private boolean playerHasEnough(Player player, int amount)
	{
		return player.getInventory().contains(this.item, amount);
	}

	/**
	 * Get shop by material
	 * 
	 * @param material to search
	 * @return list of shopsign
	 */
	public static ArrayList<ShopSign> GetByMaterial(String material)
	{
		ArrayList<ShopSign> shops = new ArrayList<ShopSign>();
		try
		{
			PreparedStatement st = Config.GetConfig().getConnection().prepareStatement(
					"Select worldUUID, x, y, z, ownerUUID, buy, sell, item, amount, amountMax, currency from "
							+ TABLE_NAME + " where item LIKE ?;");
			st.setString(1, material);
			ResultSet res = st.executeQuery();
			while (res.next())
			{
				shops.add(new ShopSign(res.getString("worldUUID"), res.getInt("x"), res.getInt("y"), res.getInt("z"),
						res.getString("ownerUUID"), res.getDouble("buy"), res.getDouble("sell"), res.getString("item"),
						res.getInt("amount"), res.getInt("amountMax"), res.getInt("currency")));
			}
			st.close();
			res.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
		return shops;
	}

	public static String getSignLine(Sign sign, int line)
	{
		return sign.getSide(Side.FRONT).getLine(line);
	}

	public static void setSignLine(Sign sign, int line, String text)
	{
		sign.getSide(Side.FRONT).setLine(line, text);
	}
}
