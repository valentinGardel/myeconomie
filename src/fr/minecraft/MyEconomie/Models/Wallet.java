package fr.minecraft.MyEconomie.Models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;

import ActiveRecord.Model;
import fr.minecraft.MyEconomie.Config;

/**
 * Wallet of currencies of a player
 */
public class Wallet extends Model
{
	private static final String TABLE_NAME = "Wallet";
	public static final int PAGE_SIZE = 10;

	/**
	 * Currencies in the account
	 */
	private HashMap<Currency, Double> currencies;

	/**
	 * Player who own the wallet
	 */
	public OfflinePlayer player;

	/**
	 * @param player who own the wallet
	 */
	private Wallet(OfflinePlayer player)
	{
		this.player = player;
		this.currencies = new HashMap<Currency, Double>();
	}

	/**
	 * Create the table in the database if not exists
	 */
	public static void CreateTable()
	{
		try
		{
			Statement stmt = Config.GetConfig().getConnection().createStatement();
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS " + TABLE_NAME
					+ "(playerUUID varchar(50), currency int REFERENCES " + Currency.TABLE_NAME
					+ "(id) ON DELETE CASCADE, amount DECIMAL(19, 2), PRIMARY KEY (playerUUID, currency));");
			stmt.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
	}

	/**
	 * Get a wallet of a player
	 * 
	 * @param player who own the wallet
	 */
	public static Wallet GetByPlayer(OfflinePlayer player)
	{
		Wallet wallet = new Wallet(player);

		try
		{
			PreparedStatement st = Config.GetConfig().getConnection().prepareStatement(
					"Select playerUUID, currency, amount from " + TABLE_NAME + " where playerUUID=?;");
			st.setString(1, player.getUniqueId().toString());
			ResultSet res = st.executeQuery();

			while (res.next())
			{
				wallet.setCurrencie(Currency.GetById(res.getInt("currency")), res.getDouble("amount"));
			}

			st.close();
			res.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}

		return wallet;
	}

	/**
	 * Add an amount of currency in this wallet
	 * 
	 * @param currency to add
	 * @param amount   to add
	 */
	public boolean addCurrency(Currency currency, double amount)
	{
		// Bukkit.broadcastMessage("Amount: "+amount);
		double newAmount = ((amount * 100) + (this.getCurrency(currency)) * 100) / 100.0;
		// Bukkit.broadcastMessage("New amount: "+newAmount);
		try
		{
			// update in db
			if (this.currencies.containsKey(currency))
			{
				PreparedStatement st = Config.GetConfig().getConnection()
						.prepareStatement("update " + TABLE_NAME + " set amount=? where playerUUID=? and currency=?;");
				st.setDouble(1, newAmount);
				st.setString(2, this.player.getUniqueId().toString());
				st.setInt(3, currency.getId());
				st.executeUpdate();
				st.close();
			}
			// insert in db
			else
			{
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement(
						"insert into " + TABLE_NAME + " (playerUUID, currency, amount) values(?, ?, ?);");
				st.setString(1, this.player.getUniqueId().toString());
				st.setInt(2, currency.getId());
				st.setDouble(3, newAmount);
				st.executeUpdate();
				st.close();
			}
			this.currencies.put(currency, newAmount);
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
			return false;
		}
		return true;
	}

	public boolean removeCurrency(Currency currency, double amount)
	{
		return this.addCurrency(currency, -amount);
	}

	public boolean setCurrency(Currency currency, double amount)
	{
		return this.addCurrency(currency, -this.getCurrency(currency) + amount);
	}

	/**
	 * Set the currency (not in db)
	 * 
	 * @param currency to update
	 * @param amount   to set
	 */
	private void setCurrencie(Currency currency, double amount)
	{
		if (currency != null)
		{
			this.currencies.put(currency, amount);
		}
	}

	/**
	 * Check if wallet can pay
	 * 
	 * @param currency to check
	 * @param amount   to check
	 */
	public boolean canPay(Currency currency, double amount)
	{
		return this.getCurrency(currency) >= amount;
	}

	/**
	 * Transfer from a wallet to another if not enough currency amount will be
	 * negative (check with canPay method before)
	 * 
	 * @param walletFrom
	 * @param walletTo
	 * @param currency   to transfer
	 * @param amount     to transfer
	 */
	public static boolean Transfer(Wallet walletFrom, Wallet walletTo, Currency currency, double amount)
	{
		if (walletFrom.removeCurrency(currency, amount))
		{
			if (!walletTo.addCurrency(currency, amount))
			{
				walletFrom.addCurrency(currency, amount);
				return false;
			}
		} else
			return false;
		return true;
	}

	/**
	 * @return string who describe the wallet
	 */
	public String toString()
	{
		String res = ChatColor.GOLD + "Compte bancaire:" + ChatColor.WHITE;

		Set<Currency> keys = this.currencies.keySet();
		for (Currency currency : keys)
		{
			res += "\n" + this.toString(currency);
		}

		return res;
	}

	/**
	 * @return string who describe the state of a currency in this wallet
	 */
	public String toString(Currency currency)
	{
		Double amount = this.currencies.containsKey(currency) ? this.currencies.get(currency) : 0;
		return "Solde de " + currency.name + ": " + amount + " " + currency.symbol;
	}

	/**
	 * Get currency leaderboard
	 * 
	 * @param currency of the leaderboard to get
	 * @param page     of leaderboard to get
	 */
	public static List<String> GetLeaderboard(Currency currency, int page)
	{
		ArrayList<String> lignes = new ArrayList<String>();
		try
		{
			int i = page * PAGE_SIZE;
			PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("SELECT playerUUID, amount FROM "
					+ TABLE_NAME + " WHERE currency=? ORDER BY amount DESC LIMIT ?, ? ;");
			st.setInt(1, currency.getId());
			st.setInt(2, i);
			st.setInt(3, PAGE_SIZE);
			ResultSet res = st.executeQuery();

			while (res.next())
			{
				i++;
				lignes.add(i + " - " + Bukkit.getOfflinePlayer(UUID.fromString(res.getString("playerUUID"))).getName()
						+ ": " + res.getDouble("amount") + currency.symbol);
			}
			st.close();
			res.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
			lignes = null;
			;
		}
		return lignes;
	}

	/**
	 * @param currency to check the amount
	 * @return the amount of currency in the wallet
	 */
	public double getCurrency(Currency currency)
	{
		if (this.currencies.containsKey(currency))
			return Double.valueOf(this.currencies.get(currency));
		else
			return Double.valueOf(0);
	}
}
