package fr.minecraft.MyEconomie.Models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Sign;

import ActiveRecord.Model;
import fr.minecraft.MyEconomie.Config;
import fr.minecraft.MyEconomie.Utils.Code;

public class CustomShop extends Model
{
	public static final String FIRST_LINE = "[CUSTOM]", FIRST_LINE_DISABLE = ChatColor.RED + "[CUSTOM]",
			CUSTOM_SHOP_TABLE = "`CustomShop`", CUSTOM_SHOP_COMMAND = "`CustomShopCommands`",
			CUSTOM_SHOP_REQUIRE = "`CustomShopRequires`", CUSTOM_SHOP_FORBIDDEN = "`CustomShopForbiddens`";
	public static int LINE_MAX_LENGTH = 15;

	public Currency currency;
	public double buy;
	public Location location;
	public ArrayList<String> commands, requires, forbiddens;
	public String description;
	public boolean breakAfterBuyed;
	private int id = -1;
	private boolean inDatabase;

	public CustomShop(Location location, Currency currency, double buy)
	{
		this.location = location;
		this.currency = currency;
		this.buy = buy;
		this.commands = new ArrayList<String>();
		this.requires = new ArrayList<String>();
		this.forbiddens = new ArrayList<String>();
		this.description = "";
		this.breakAfterBuyed = false;
		this.inDatabase = false;
	}

	/**
	 * Private constructor used when retrieved from database
	 */
	private CustomShop(int id, String worldUUID, int x, int y, int z, double buy, int currencyId, String description,
			boolean breakAfterBuyed, ArrayList<String> requires, ArrayList<String> forbiddens,
			ArrayList<String> commands)
	{
		this.id = id;
		this.location = new Location(Bukkit.getWorld(UUID.fromString(worldUUID)), x, y, z);
		this.buy = buy;
		this.currency = Currency.GetById(currencyId);
		this.description = description;
		this.requires = requires;
		this.forbiddens = forbiddens;
		this.commands = commands;
		this.breakAfterBuyed = breakAfterBuyed;
		this.inDatabase = true;
	}

	/**
	 * Creation of database tables if not exists
	 */
	public static void CreateTable()
	{
		try
		{
			Statement stmt = Config.GetConfig().getConnection().createStatement();
			stmt.executeUpdate("create table if not exists " + CUSTOM_SHOP_TABLE
					+ "(id int auto_increment PRIMARY KEY, worldUUID varchar(50), x int, y int, z int, buy decimal(19,2), breakAfterBuyed boolean, description varchar("
					+ (LINE_MAX_LENGTH * 2) + "), currency int REFERENCES " + Currency.TABLE_NAME
					+ "(id) ON DELETE CASCADE, UNIQUE (worldUUID, x, y, z));");
			stmt.executeUpdate("create table if not exists " + CUSTOM_SHOP_COMMAND
					+ "(id int auto_increment PRIMARY KEY, shop_id int REFERENCES " + CUSTOM_SHOP_TABLE
					+ "(id) ON DELETE CASCADE, command varchar(256));");
			stmt.executeUpdate("create table if not exists " + CUSTOM_SHOP_REQUIRE
					+ "(id int auto_increment PRIMARY KEY, shop_id int REFERENCES " + CUSTOM_SHOP_TABLE
					+ "(id) ON DELETE CASCADE, `require` varchar(256));");
			stmt.executeUpdate("create table if not exists " + CUSTOM_SHOP_FORBIDDEN
					+ "(id int auto_increment PRIMARY KEY, shop_id int REFERENCES " + CUSTOM_SHOP_TABLE
					+ "(id) ON DELETE CASCADE, forbidden varchar(256));");
			stmt.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
	}

	/**
	 * Save the shop in the database
	 * 
	 * @return Code Failed or Success
	 */
	public Code save()
	{
		try
		{
			// update in db
			if (this.inDatabase)
			{
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("update " + CUSTOM_SHOP_TABLE
						+ " set buy=?, currency=?, description=?, breakAfterBuyed=? where worldUUID=? and x=? and y=? and z=?;");
				st.setDouble(1, this.buy);
				st.setInt(2, this.currency.getId());
				st.setString(3, this.description);
				st.setBoolean(4, this.breakAfterBuyed);
				st.setString(5, this.location.getWorld().getUID().toString());
				st.setInt(6, this.location.getBlockX());
				st.setInt(7, this.location.getBlockY());
				st.setInt(8, this.location.getBlockZ());
				st.executeUpdate();
				st.close();
			}
			// insert in db
			else
			{
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("insert into "
						+ CUSTOM_SHOP_TABLE
						+ " (worldUUID, x, y, z, buy, currency, description, breakAfterBuyed) values(?, ?, ?, ?, ?, ?, ?, ?);");

				st.setString(1, this.location.getWorld().getUID().toString());
				st.setInt(2, this.location.getBlockX());
				st.setInt(3, this.location.getBlockY());
				st.setInt(4, this.location.getBlockZ());
				st.setDouble(5, this.buy);
				st.setInt(6, this.currency.getId());
				st.setString(7, this.description);
				st.setBoolean(8, this.breakAfterBuyed);
				st.executeUpdate();
				st.close();
			}
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
			return Code.Failed;
		}
		return Code.Success;
	}

	public Code addRequire(String require)
	{
		try
		{
			if (this.inDatabase)
			{
				PreparedStatement st = Config.GetConfig().getConnection()
						.prepareStatement("insert into " + CUSTOM_SHOP_REQUIRE + " (shop_id, `require`) values(?, ?);");

				st.setInt(1, this.id);
				st.setString(2, require);
				st.executeUpdate();
				st.close();
			}
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
			return Code.Failed;
		}
		return Code.Success;
	}

	public Code addForbidden(String forbidden)
	{
		try
		{
			if (this.inDatabase)
			{
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement(
						"insert into " + CUSTOM_SHOP_FORBIDDEN + " (shop_id, forbidden) values(?, ?);");

				st.setInt(1, this.id);
				st.setString(2, forbidden);
				st.executeUpdate();
				st.close();
			}
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
			return Code.Failed;
		}
		return Code.Success;
	}

	public Code addCommand(String command)
	{
		try
		{
			if (this.inDatabase)
			{
				PreparedStatement st = Config.GetConfig().getConnection()
						.prepareStatement("insert into " + CUSTOM_SHOP_COMMAND + " (shop_id, command) values(?, ?);");

				st.setInt(1, this.id);
				st.setString(2, command);
				st.executeUpdate();
				st.close();
			}
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
			return Code.Failed;
		}
		return Code.Success;
	}

	public Code removeRequire(String require)
	{
		try
		{
			// update in db
			if (this.inDatabase)
			{
				PreparedStatement st = Config.GetConfig().getConnection()
						.prepareStatement("DELETE FROM " + CUSTOM_SHOP_REQUIRE + " WHERE shop_id=? and `require`=?;");
				st.setInt(1, this.id);
				st.setString(2, require);
				st.executeUpdate();
				st.close();
				this.inDatabase = false;
			} else
				return Code.NotInDatabase;
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
			return Code.Failed;
		}
		return Code.Success;
	}

	public Code removeForbidden(String forbidden)
	{
		try
		{
			// update in db
			if (this.inDatabase)
			{
				PreparedStatement st = Config.GetConfig().getConnection()
						.prepareStatement("DELETE FROM " + CUSTOM_SHOP_FORBIDDEN + " WHERE shop_id=? and forbidden=?;");
				st.setInt(1, this.id);
				st.setString(2, forbidden);
				st.executeUpdate();
				st.close();
				this.inDatabase = false;
			} else
				return Code.NotInDatabase;
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
			return Code.Failed;
		}
		return Code.Success;
	}

	public Code removeCommand(String command)
	{
		try
		{
			// update in db
			if (this.inDatabase)
			{
				PreparedStatement st = Config.GetConfig().getConnection()
						.prepareStatement("DELETE FROM " + CUSTOM_SHOP_COMMAND + " WHERE shop_id=? and command=?;");
				st.setInt(1, this.id);
				st.setString(2, command);
				st.executeUpdate();
				st.close();
				this.inDatabase = false;
			} else
				return Code.NotInDatabase;
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
			return Code.Failed;
		}
		return Code.Success;
	}

	/**
	 * Delete the shop in the database
	 * 
	 * @return Code Failed or Success
	 */
	public Code delete()
	{
		try
		{
			// update in db
			if (this.inDatabase)
			{
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement(
						"DELETE FROM " + CUSTOM_SHOP_TABLE + " WHERE worldUUID=? and x=? and y=? and z=?;");
				st.setString(1, this.location.getWorld().getUID().toString());
				st.setInt(2, this.location.getBlockX());
				st.setInt(3, this.location.getBlockY());
				st.setInt(4, this.location.getBlockZ());
				st.executeUpdate();
				st.close();
				this.inDatabase = false;
			} else
				return Code.NotInDatabase;
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
			return Code.Failed;
		}
		return Code.Success;
	}

	/**
	 * Retieve the shop at the given location
	 * 
	 * @param location of the shop to retrieve
	 * @return ShopSign
	 */
	public static CustomShop GetByLocation(Location location)
	{
		CustomShop sign = null;
		try
		{
			PreparedStatement st = Config.GetConfig().getConnection()
					.prepareStatement("Select worldUUID, x, y, z, buy, currency, description, id, breakAfterBuyed from "
							+ CUSTOM_SHOP_TABLE + " where worldUUID=? and x=? and y=? and z=?;");
			st.setString(1, location.getWorld().getUID().toString());
			st.setInt(2, location.getBlockX());
			st.setInt(3, location.getBlockY());
			st.setInt(4, location.getBlockZ());
			ResultSet res = st.executeQuery();
			if (res.next())
			{
				sign = new CustomShop(res.getInt("id"), res.getString("worldUUID"), res.getInt("x"), res.getInt("y"),
						res.getInt("z"), res.getDouble("buy"), res.getInt("currency"), res.getString("description"),
						res.getBoolean("breakAfterBuyed"), GetRequires(res.getInt("id")),
						GetForbiddens(res.getInt("id")), GetCommands(res.getInt("id"))

				);
			}
			st.close();
			res.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
		return sign;
	}

	private static ArrayList<String> GetRequires(int id)
	{
		ArrayList<String> requires = new ArrayList<String>();
		try
		{
			PreparedStatement st = Config.GetConfig().getConnection()
					.prepareStatement("Select `require` from " + CUSTOM_SHOP_REQUIRE + " where shop_id=?;");
			st.setInt(1, id);
			ResultSet res = st.executeQuery();
			while (res.next())
			{
				requires.add(res.getString("require"));
			}
			st.close();
			res.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
		return requires;
	}

	private static ArrayList<String> GetForbiddens(int id)
	{
		ArrayList<String> forbiddens = new ArrayList<String>();
		try
		{
			PreparedStatement st = Config.GetConfig().getConnection()
					.prepareStatement("Select forbidden from " + CUSTOM_SHOP_FORBIDDEN + " where shop_id=?;");
			st.setInt(1, id);
			ResultSet res = st.executeQuery();
			while (res.next())
			{
				forbiddens.add(res.getString("forbidden"));
			}
			st.close();
			res.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
		return forbiddens;
	}

	private static ArrayList<String> GetCommands(int id)
	{
		ArrayList<String> commands = new ArrayList<String>();
		try
		{
			PreparedStatement st = Config.GetConfig().getConnection()
					.prepareStatement("Select command from " + CUSTOM_SHOP_COMMAND + " where shop_id=?;");
			st.setInt(1, id);
			ResultSet res = st.executeQuery();
			while (res.next())
			{
				commands.add(res.getString("command"));
			}
			st.close();
			res.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
		return commands;
	}

	/**
	 * Update the sign lines with the right description
	 */
	public void updateSign()
	{
		Sign sign = (Sign) this.location.getBlock().getState();

		ShopSign.setSignLine(sign, 1, this.buy + this.currency.symbol);
		if (!this.description.isEmpty())
		{
			String line3 = this.description.substring(0,
					this.description.length() >= LINE_MAX_LENGTH ? LINE_MAX_LENGTH : this.description.length());
			ShopSign.setSignLine(sign, 2, line3);

			if (this.description.length() > LINE_MAX_LENGTH)
			{
				String line4 = this.description.substring(LINE_MAX_LENGTH, this.description.length());
				ShopSign.setSignLine(sign, 3, line4);
			}
		}
		sign.update();
	}

	/**
	 * Do what the shop need to do after been bought (update, break ...)
	 */
	public void hasBeenBuyed()
	{
		if (this.breakAfterBuyed)
		{
			this.delete();
			this.location.getBlock().breakNaturally(null);
		}
	}
}
