package fr.minecraft.MyEconomie.Models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import ActiveRecord.Model;
import fr.minecraft.MyEconomie.Config;
import fr.minecraft.MyEconomie.Utils.Code;

public class Currency extends Model
{
	public static final String TABLE_NAME = "Currency";

	private int id = -1;
	public String name, symbol;
	public boolean isDefault, tradable;

	/**
	 * @param name      of the currency
	 * @param symbol    of the currency
	 * @param isDefault if currency is the default one (can only have one)
	 * @param tradable  if players can trade it with other
	 */
	public Currency(String name, String symbol, boolean isDefault, boolean tradable)
	{
		this.name = name;
		this.symbol = symbol;
		this.isDefault = isDefault;
		this.tradable = tradable;
	}

	private Currency(int id, String name, String symbol, boolean isDefault, boolean tradable)
	{
		this(name, symbol, isDefault, tradable);
		this.id = id;
	}

	public int getId()
	{
		return this.id;
	}

	public static void CreateTable()
	{
		try
		{
			Statement stmt = Config.GetConfig().getConnection().createStatement();
			stmt.executeUpdate("create table if not exists " + TABLE_NAME
					+ "(id int PRIMARY KEY AUTO_INCREMENT, name varchar(32) UNIQUE, symbol varchar(16) UNIQUE, isDefault bool, tradable bool);");
			stmt.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
	}

	public Code save()
	{
		try
		{
			// update in db
			if (this.id >= 0)
			{
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement(
						"update " + TABLE_NAME + " set name=?, symbol=?, isDefault=?, tradable=? where id=?;");
				st.setString(1, this.name);
				st.setString(2, this.symbol);
				st.setBoolean(3, this.isDefault);
				st.setBoolean(4, this.tradable);
				st.setInt(5, this.id);
				st.executeUpdate();
				st.close();
			}
			// insert in db
			else
			{
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement(
						"insert into " + TABLE_NAME + " (name, symbol, isDefault, tradable) values(?, ?, ?, ?);");
				st.setString(1, this.name);
				st.setString(2, this.symbol);
				st.setBoolean(3, this.isDefault);
				st.setBoolean(4, this.tradable);
				st.executeUpdate();
				st.close();
			}
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
			return Code.Failed;
		}
		return Code.Success;
	}

	public static Currency GetDefault()
	{
		Currency currency = null;

		try
		{
			PreparedStatement st = Config.GetConfig().getConnection().prepareStatement(
					"Select id, name, symbol, isDefault, tradable from " + TABLE_NAME + " where isDefault;");

			ResultSet res = st.executeQuery();
			if (res.next())
			{
				currency = new Currency(res.getInt("id"), res.getString("name"), res.getString("symbol"),
						res.getBoolean("isDefault"), res.getBoolean("tradable"));
			}
			st.close();
			res.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
		return currency;
	}

	public static Currency GetById(int id)
	{
		Currency currency = null;

		try
		{
			PreparedStatement st = Config.GetConfig().getConnection().prepareStatement(
					"Select id, name, symbol, isDefault, tradable from " + TABLE_NAME + " where id=?;");
			st.setInt(1, id);
			ResultSet res = st.executeQuery();
			if (res.next())
			{
				currency = new Currency(res.getInt("id"), res.getString("name"), res.getString("symbol"),
						res.getBoolean("isDefault"), res.getBoolean("tradable"));
			}
			st.close();
			res.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
		return currency;
	}

	public static Currency GetByName(String name)
	{
		return Currency.GetBy(name, "name");
	}

	public static Currency GetBySymbol(String symbol)
	{
		return Currency.GetBy(symbol, "symbol");
	}

	private static Currency GetBy(String value, String field)
	{
		Currency currency = null;

		try
		{
			PreparedStatement st = Config.GetConfig().getConnection().prepareStatement(
					"Select id, name, symbol, isDefault, tradable from " + TABLE_NAME + " where " + field + "=?;");
			st.setString(1, value);
			ResultSet res = st.executeQuery();
			if (res.next())
			{
				currency = new Currency(res.getInt("id"), res.getString("name"), res.getString("symbol"),
						res.getBoolean("isDefault"), res.getBoolean("tradable"));
			}
			st.close();
			res.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
		return currency;
	}

	@Override
	public int hashCode()
	{
		return this.id;
	}

	@Override
	public boolean equals(Object o)
	{
		return ((o == this) || (o instanceof Currency && this.id > 0 && ((Currency) o).id == this.id));
	}

	public static List<Currency> GetAll()
	{
		List<Currency> currencies = new ArrayList<Currency>();
		try
		{
			PreparedStatement st = Config.GetConfig().getConnection()
					.prepareStatement("Select id, name, symbol, isDefault, tradable from " + TABLE_NAME + ";");
			ResultSet res = st.executeQuery();
			while (res.next())
			{
				currencies.add(new Currency(res.getInt("id"), res.getString("name"), res.getString("symbol"),
						res.getBoolean("isDefault"), res.getBoolean("tradable")));
			}
			st.close();
			res.close();
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
		return currencies;
	}

	public Code delete()
	{
		try
		{
			// update in db
			if (this.id != -1)
			{
				PreparedStatement st = Config.GetConfig().getConnection()
						.prepareStatement("DELETE FROM " + TABLE_NAME + " WHERE id=?;");
				st.setInt(1, this.id);
				st.executeUpdate();
				st.close();
			} else
				return Code.NotInDatabase;
		} catch (SQLException e)
		{
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
			return Code.Failed;
		}
		return Code.Success;
	}

	public String toString()
	{
		String res = this.name + ", symbol: " + this.symbol;
		res += this.isDefault ? " est default" : " n'est pas default,";
		res += this.tradable ? " est échangeable" : " n'est pas échangeable";
		return res;
	}

	public static Currency GetByNameOrSymbol(String string)
	{
		Currency res = null;
		res = GetByName(string);
		if (res == null)
			res = GetBySymbol(string);
		return res;
	}
}
