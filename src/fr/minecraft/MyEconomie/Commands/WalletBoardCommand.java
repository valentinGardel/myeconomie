package fr.minecraft.MyEconomie.Commands;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftCommand;
import Commons.MessageBuilder;
import fr.minecraft.MyEconomie.Models.Currency;
import fr.minecraft.MyEconomie.Models.Wallet;
import fr.minecraft.MyEconomie.TabCompleters.WalletBoardTabCompleter;

public class WalletBoardCommand extends MinecraftCommand
{
	static
	{
		COMMAND_NAME = "walletboard";
		TAB_COMPLETER = WalletBoardTabCompleter.class;
	}

	/**
	 * Command /walletboard [<currency>] [<page>]
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (args.length <= 2)
		{
			Currency currency;
			if (args.length == 0)
				currency = Currency.GetDefault();
			else
				currency = Currency.GetByNameOrSymbol(args[0]);

			if (currency != null)
			{
				int page = 0;
				if (args.length == 2)
				{
					try
					{
						page = Integer.parseInt(args[1]) - 1;
						if (page < 0)
							throw new NumberFormatException();
					} catch (NumberFormatException e)
					{
						sender.sendMessage(ChatColor.RED + "Le numéro de page est invalide!");
						return true;
					}
				}
				this.displayLeaderboard(sender, currency, page);
			} else
			{
				String currencyName = (args.length > 0) ? args[0] : "par defaut";
				sender.sendMessage(ChatColor.RED + "La monnaie " + currencyName + " est introuvable!");
			}
		} else
			sender.sendMessage(ChatColor.RED + "Commande invalide!");
		return true;
	}

	private void displayLeaderboard(CommandSender sender, Currency currency, int page)
	{
		List<String> lignes = Wallet.GetLeaderboard(currency, page);
		if (lignes != null)
		{
			MessageBuilder message = MessageBuilder.create(ChatColor.GOLD + "Leaderboard de la monnaie " + currency.name
					+ " (page " + (page + 1) + "):" + ChatColor.WHITE + "\n");
			for (String ligne : lignes)
			{
				message.write(ligne + "\n");
			}
			if (page != 0)
				message.onClickRunCommand("/walletboard " + currency.name + " " + (page - 1))
						.write(ChatColor.GOLD + "<<<Precedent");
			else
				message.write(ChatColor.GRAY + "<<<Precedent");
			message.write(" | ");
			if (lignes.size() == Wallet.PAGE_SIZE)
				message.onClickRunCommand("/walletboard " + currency.name + " " + (page + 1))
						.write(ChatColor.GOLD + "Suivant>>>");
			else
				message.write(ChatColor.GRAY + "Suivant>>>");
			message.send(sender);
		} else
			sender.sendMessage(ChatColor.RED + "Impossible de charger le leaderboard!");
	}
}
