package fr.minecraft.MyEconomie.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyEconomie.Models.Currency;
import fr.minecraft.MyEconomie.Models.Wallet;
import fr.minecraft.MyEconomie.TabCompleters.PayTabCompleter;

public class PayCommand extends MinecraftCommand
{
	static
	{
		COMMAND_NAME = "pay";
		TAB_COMPLETER = PayTabCompleter.class;
	}

	/**
	 * Command /pay [player] [amount] [<currency>]
	 */
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (sender instanceof Player)
		{
			Player player = (Player) sender;
			if (args.length == 3 || args.length == 2)
			{
				try
				{
					double amount = Double.valueOf(Math.round(Double.parseDouble(args[1]) * 100) / Double.valueOf(100));
					if (amount > 0)
					{
						OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
						if (target != null && target.hasPlayedBefore())
						{
							Currency currency = null;
							if (args.length == 3)
							{
								currency = Currency.GetByNameOrSymbol(args[2]);
								if (currency == null)
									player.sendMessage(ChatColor.RED + "La monnaie " + args[2] + " est introuvable!");
							} else
								currency = Currency.GetDefault();

							if (currency != null)
								this.onPay(player, target, amount, currency);
							else
								player.sendMessage(ChatColor.RED + "La monnaie par defaut est introuvable!");
						} else
							player.sendMessage(ChatColor.RED + "Le joueur " + args[0] + " est introuvable!");
					} else
						player.sendMessage(
								ChatColor.RED + "Vous ne pouvez pas donner une valeur inférieur ou égal à zéro!");
				} catch (NumberFormatException e)
				{
					player.sendMessage(ChatColor.RED + "Le montant n'est pas valide!");
				}
			} else
				sender.sendMessage(ChatColor.RED + "Commande invalide!");
		} else
			sender.sendMessage(ChatColor.RED + "Commande impossible pour un non joueur!");
		return true;
	}

	/**
	 * Make the currency transfer
	 * 
	 * @param player   who want to pay
	 * @param target   player who get payed
	 * @param amount   payed
	 * @param currency used
	 */
	private void onPay(Player player, OfflinePlayer target, double amount, Currency currency)
	{
		if (currency.tradable)
		{
			Wallet walletFrom = Wallet.GetByPlayer(player);
			if (walletFrom != null && walletFrom.canPay(currency, amount))
			{
				Wallet walletTo = Wallet.GetByPlayer(target);
				if (walletTo != null)
				{
					if (Wallet.Transfer(walletFrom, walletTo, currency, amount))
					{
						player.sendMessage(ChatColor.GREEN + "Transfert de " + amount + currency.symbol + " à "
								+ target.getName() + " effectué!");
						if (target.isOnline())
							target.getPlayer().sendMessage(ChatColor.GREEN + "Vous avez recu un transfert de " + amount
									+ currency.symbol + " de la part de " + player.getName() + ".");
					} else
						player.sendMessage(ChatColor.RED + "Le transfert a échoué!");
				} else
					player.sendMessage(
							ChatColor.RED + "Error, can't get the wallet of the target, please report to the admin!");
			} else
				player.sendMessage(ChatColor.RED + "Votre solde est insuffisant!");
		} else
			player.sendMessage(ChatColor.RED + "La monnaie " + currency.name + " n'est pas échangeable!");
	}
}
