package fr.minecraft.MyEconomie.Commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyEconomie.Controllers.ShopController;
import fr.minecraft.MyEconomie.Models.ShopSign;
import fr.minecraft.MyEconomie.TabCompleters.ShopTabCompleter;

public class ShopCommand extends MinecraftCommand
{
	static
	{
		COMMAND_NAME = "shop";
		TAB_COMPLETER = ShopTabCompleter.class;
	}

	/**
	 * Command /shop [take|store] [amount]
	 **/
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (sender instanceof Player)
		{
			Player player = (Player) sender;
			if (args.length == 2)
			{
				this.onUse(player, args);
			} else if (args.length == 3 && args[0].equalsIgnoreCase("update"))
			{
				this.onUpdate(player, args);
			} else
				player.sendMessage(ChatColor.RED + "Commande invalide!");
		} else
			sender.sendMessage(ChatColor.RED + "Commande impossible pour un non-joueur!");
		return true;
	}

	private void onUse(Player player, String[] args)
	{
		int amount = -1;
		try
		{
			amount = Integer.parseInt(args[1]);
		} catch (NumberFormatException e)
		{
			player.sendMessage(ChatColor.RED + "Montant invalide!");
			return;
		}
		if (amount > 0)
		{
			if (args[0].equalsIgnoreCase("take"))
			{
				ShopController.Take(player, amount);
			} else if (args[0].equalsIgnoreCase("store"))
			{
				ShopController.Store(player, amount);
			} else
				player.sendMessage(ChatColor.RED + "Commande invalide!");
		} else
			player.sendMessage(ChatColor.RED + "Le montant ne peut pas étre inférieur ou égal à zéro!");
	}

	private void onUpdate(Player player, String[] args)
	{
		Location location = ShopController.Get(player);
		if (location != null)
		{
			ShopSign sign = ShopSign.GetByLocation(location);
			if (sign != null)
			{
				double amount = -1;
				try
				{
					amount = Math.round(Double.parseDouble(args[2]) * 100) / 100.0;
					if (amount >= 0)
					{
						switch (args[1].toUpperCase())
						{
						case "QUANTITY":
							sign.maxQuantity = (int) Math.round(amount);
							break;
						case "BUY":
							sign.buy = amount;
							break;
						case "SELL":
							sign.sell = amount;
							break;
						default:
							player.sendMessage(ChatColor.RED + "Commande invalide!");
							return;
						}
						switch (sign.save())
						{
						case Success:
							sign.updateSign();
							player.sendMessage(ChatColor.GREEN + "Shop modifié!");
							break;
						default:
							player.sendMessage(ChatColor.RED + "Error, please contact an admin!");
							break;
						}
					} else
						player.sendMessage(ChatColor.RED + "Le montant ne peut pas étre inférieur à zéro!");
				} catch (NumberFormatException e)
				{
					player.sendMessage(ChatColor.RED + "Montant invalide");
				}
			} else
				player.sendMessage(ChatColor.RED + "Le shop est introuvable!");
		} else
			player.sendMessage(ChatColor.RED + "Vous n'avez pas selectionné de shop!");
	}
}
