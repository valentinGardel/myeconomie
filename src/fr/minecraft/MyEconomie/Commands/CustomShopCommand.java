package fr.minecraft.MyEconomie.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyEconomie.Controllers.CustomShopController;
import fr.minecraft.MyEconomie.Models.Currency;
import fr.minecraft.MyEconomie.TabCompleters.CustomShopTabCompleter;

public class CustomShopCommand extends MinecraftCommand
{
	static
	{
		COMMAND_NAME = "customshop";
		TAB_COMPLETER = CustomShopTabCompleter.class;
	}

	/**
	 * customshop [add | remove] [require | forbidden | command ] [value] customshop
	 * update [buy | description | currency | breakAfterBuyed] [value]
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (sender instanceof Player)
		{
			Player player = (Player) sender;
			if (args.length >= 3)
			{
				switch (args[0].toUpperCase())
				{
				case "ADD":
					this.onAddArg(player, args);
					break;
				case "REMOVE":
					this.onRemoveArg(player, args);
					break;
				case "UPDATE":
					this.onUpdateArg(player, args);
					break;
				default:
					player.sendMessage(ChatColor.RED + "Commande invalide!");
					break;
				}
			} else if (args.length == 0)
				CustomShopController.Info(player);
			else
				player.sendMessage(ChatColor.RED + "Commande invalide!");
		} else
			sender.sendMessage(ChatColor.RED + "Commande imposible pour un non-joueur!");
		return true;
	}

	private void onAddArg(Player player, String[] args)
	{
		switch (args[1].toUpperCase())
		{
		case "REQUIRE":
			CustomShopController.AddRequire(player, args[2]);
			break;
		case "FORBIDDEN":
			CustomShopController.AddForbidden(player, args[2]);
			break;
		case "COMMAND":
			String command = "";
			for (int i = 2; i < args.length; i++)
				command += args[i] + " ";
			CustomShopController.AddCommand(player, command);
			break;
		default:
			player.sendMessage(ChatColor.RED + "Commande invalide!");
			break;
		}
	}

	private void onRemoveArg(Player player, String[] args)
	{
		switch (args[1].toUpperCase())
		{
		case "REQUIRE":
			CustomShopController.RemoveRequire(player, args[2]);
			break;
		case "FORBIDDEN":
			CustomShopController.RemoveForbidden(player, args[2]);
			break;
		case "COMMAND":
			String command = "";
			for (int i = 2; i < args.length; i++)
				command += args[i] + " ";
			CustomShopController.RemoveCommand(player, command);
			break;
		default:
			player.sendMessage(ChatColor.RED + "Commande invalide!");
			break;
		}
	}

	private void onUpdateArg(Player player, String[] args)
	{
		switch (args[1].toUpperCase())
		{
		case "BUY":
			try
			{
				double buy = Double.parseDouble(args[2]);
				buy = Math.round(buy * 100.0) / 100.0;
				CustomShopController.UpdateBuy(player, buy);
			} catch (NumberFormatException e)
			{
				player.sendMessage(ChatColor.RED + "Le prix est invalide!");
			}
			break;
		case "DESCRIPTION":
			String description = "";
			for (int i = 2; i < args.length; i++)
				description += args[i] + " ";
			CustomShopController.UpdateDescription(player, description);
			break;
		case "CURRENCY":
			Currency currency = Currency.GetByNameOrSymbol(args[2]);
			if (currency != null)
				CustomShopController.UpdateCurrency(player, currency);
			else
				player.sendMessage(ChatColor.RED + "Monnaie invalide!");
			break;
		case "BREAKAFTERBUYED":
			boolean value = Boolean.parseBoolean(args[2]);
			CustomShopController.UpdateBreakAfterBuyed(player, value);
			break;
		default:
			player.sendMessage(ChatColor.RED + "Commande invalide!");
			break;
		}
	}
}
