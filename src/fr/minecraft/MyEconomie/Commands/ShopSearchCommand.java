package fr.minecraft.MyEconomie.Commands;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyEconomie.Controllers.ShopController;
import fr.minecraft.MyEconomie.Models.ShopSign;
import fr.minecraft.MyEconomie.TabCompleters.ShopSearchTabCompleter;

public class ShopSearchCommand extends MinecraftCommand
{
	static
	{
		COMMAND_NAME = "shopsearch";
		TAB_COMPLETER = ShopSearchTabCompleter.class;
	}

	/**
	 * Command /shopsearch [<material>]
	 **/
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (args.length == 0)
		{
			if (sender instanceof Player)
			{
				Player player = (Player) sender;
				Material material = player.getItemInHand().getType();
				if (material != null && material != Material.AIR)
				{
					if (ShopController.isSellable(material))
						this.search(sender, material);
					else
						player.sendMessage(ChatColor.RED + "Ce material n'est pas vendable/achetable!");
				} else
					sender.sendMessage(ChatColor.RED + "Vous n'avez rien en main!");
			} else
				sender.sendMessage(ChatColor.RED + "Commande invalide!");
		} else if (args.length == 1)
		{
			Material material = null;
			try
			{
				material = Material.valueOf(args[0].toUpperCase());
			} catch (IllegalArgumentException e)
			{
			}
			if (material != null)
			{
				this.search(sender, material);
			} else
				sender.sendMessage(ChatColor.RED + "Le materiel " + args[0] + " est introuvable!");
		} else
			sender.sendMessage(ChatColor.RED + "Commande invalide!");
		return true;
	}

	private void search(CommandSender sender, Material material)
	{
		ArrayList<ShopSign> shops = ShopSign.GetByMaterial(material.name());

		sender.sendMessage(ChatColor.GOLD + "" + shops.size() + " shop(s) trouvé:");
		for (ShopSign shop : shops)
		{
			if (shop.owner != null)
				sender.sendMessage(" - Vendeur: " + shop.owner.getName() + " " + this.getSellBuyFormat(shop)
						+ ", quantité: " + shop.itemQuantity + "/" + shop.maxQuantity);
			else
				sender.sendMessage(" - Vendeur: AdminShop " + this.getSellBuyFormat(shop));
		}
	}

	private String getSellBuyFormat(ShopSign shop)
	{
		String res = "";

		if (shop.buy >= 0)
			res += "vend pour " + shop.buy + shop.currency.symbol;
		if (shop.sell >= 0)
		{
			if (!res.isEmpty())
				res += ", ";
			res += "achete pour " + shop.sell + shop.currency.symbol;
		}
		return res;
	}
}
