package fr.minecraft.MyEconomie.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import Commons.MessageBuilder;
import fr.minecraft.MyEconomie.Models.Currency;
import fr.minecraft.MyEconomie.Models.Wallet;
import fr.minecraft.MyEconomie.TabCompleters.WalletTabCompleter;
import fr.minecraft.MyEconomie.Utils.Permission;

public class WalletCommand extends MinecraftCommand
{
	static
	{
		COMMAND_NAME = "wallet";
		TAB_COMPLETER = WalletTabCompleter.class;
	}

	/**
	 * Command /wallet [<currency>] [<player>] [<add|remove|set>] [<amount>]
	 */
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (args.length == 0)
		{
			if (sender instanceof Player)
			{
				Wallet wallet = Wallet.GetByPlayer((Player) sender);
				sender.sendMessage(wallet.toString());
			} else
				sender.sendMessage(ChatColor.RED + "Commande impossible pour un non-joueur!");
		} else if (args.length == 1 || args.length == 2 || args.length == 4)
		{
			if (sender.hasPermission(Permission.OTHERS_WALLET))
			{
				OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
				if (target != null)
				{
					Wallet wallet = Wallet.GetByPlayer(target);
					if (wallet != null)
					{
						if (args.length >= 2)
						{
							Currency currency = Currency.GetByNameOrSymbol(args[1]);
							if (currency != null)
							{
								if (args.length == 4)
								{
									this.onArgs4(sender, args, wallet, currency);
								} else
									this.onArgs2(sender, args, wallet, currency);
							} else
								sender.sendMessage(ChatColor.RED + "La monnaie " + args[1] + " est introuvable!");
						} else
							sender.sendMessage(wallet.toString());
					} else
						sender.sendMessage(ChatColor.RED + "Le wallet du joueur " + args[0] + " est introuvable!");
				} else
					sender.sendMessage(ChatColor.RED + "Le joueur " + args[0] + " est introuvable!");
			} else
				sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission!");
		} else
			sender.sendMessage(ChatColor.RED + "Commande invalide!");
		return true;
	}

	private void onArgs2(CommandSender sender, String[] args, Wallet wallet, Currency currency)
	{
		MessageBuilder message = MessageBuilder.create(wallet.toString(currency));
		message.onHoverText("Set").onClickSugestCommand("/wallet " + args[0] + " " + currency.name + " set [valeur]")
				.write(ChatColor.DARK_GREEN + " [Set]");
		message.onHoverText("Add").onClickSugestCommand("/wallet " + args[0] + " " + currency.name + " add [valeur]")
				.write(ChatColor.DARK_GREEN + " [Add]");
		message.onHoverText("Remove")
				.onClickSugestCommand("/wallet " + args[0] + " " + currency.name + " remove [valeur]")
				.write(ChatColor.DARK_GREEN + " [Remove]");
		message.send(sender);
	}

	private void onArgs4(CommandSender sender, String[] args, Wallet wallet, Currency currency)
	{
		if (sender.hasPermission(Permission.SET_WALLET))
		{
			double amount = -1;
			try
			{
				amount = Math.round(Double.parseDouble(args[3]) * 100) / 100;
			} catch (NumberFormatException e)
			{
				sender.sendMessage(ChatColor.RED + "Le montant de la monnaie est invalide!");
				return;
			}

			switch (args[2].toUpperCase())
			{
			case "SET":
				if (wallet.setCurrency(currency, amount))
					sender.sendMessage(ChatColor.GREEN + "Currency " + currency.name + " in wallet of "
							+ wallet.player.getName() + " set to " + wallet.getCurrency(currency) + "!");
				else
					sender.sendMessage(ChatColor.RED + "Set failed!");
				break;
			case "ADD":
				if (wallet.addCurrency(currency, amount))
					sender.sendMessage(ChatColor.GREEN + "Currency " + currency.name + " in wallet of "
							+ wallet.player.getName() + " set to " + wallet.getCurrency(currency) + "!");
				else
					sender.sendMessage(ChatColor.RED + "Add failed!");
				break;
			case "REMOVE":
				if (wallet.removeCurrency(currency, amount))
					sender.sendMessage(ChatColor.GREEN + "Currency " + currency.name + " in wallet of "
							+ wallet.player.getName() + " set to " + wallet.getCurrency(currency) + "!");
				else
					sender.sendMessage(ChatColor.RED + "Remove failed!");
				break;
			default:
				sender.sendMessage(ChatColor.RED + "Commande invalide!");
				break;
			}
		} else
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission!");
	}
}