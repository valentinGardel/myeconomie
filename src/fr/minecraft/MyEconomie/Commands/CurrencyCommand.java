package fr.minecraft.MyEconomie.Commands;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftCommand;
import fr.minecraft.MyEconomie.Models.Currency;
import fr.minecraft.MyEconomie.TabCompleters.CurrencyTabCompleter;

public class CurrencyCommand extends MinecraftCommand
{
	static
	{
		COMMAND_NAME = "currency";
		TAB_COMPLETER = CurrencyTabCompleter.class;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (args.length > 0)
		{
			switch (args[0].toUpperCase())
			{
			case "CREATE":
				this.onCreate(sender, args);
				break;
			case "UPDATE":
				this.onUpdate(sender, args);
				break;
			case "DELETE":
				this.onDelete(sender, args);
				break;
			case "LIST":
				this.onList(sender);
				break;
			default:
				sender.sendMessage(ChatColor.RED + "Commande invalide!");
				break;
			}
		} else
			sender.sendMessage(ChatColor.RED + "Vous ne pouvez pas broadcast un message vide");
		return true;
	}

	private void onList(CommandSender sender)
	{
		List<Currency> currencies = Currency.GetAll();
		sender.sendMessage(ChatColor.GOLD + "Monnaies: ");
		for (Currency currency : currencies)
		{
			sender.sendMessage(currency.toString());
		}
	}

	private void onDelete(CommandSender sender, String[] args)
	{
		if (args.length == 2)
		{
			Currency currency = Currency.GetByName(args[1]);
			if (currency != null)
			{
				switch (currency.delete())
				{
				case Failed:
					sender.sendMessage(ChatColor.RED + "Error!");
					break;
				case NotInDatabase:
					sender.sendMessage(ChatColor.RED + "La monnaie n'est pas dans la base de donnée!");
					break;
				case Success:
					sender.sendMessage(ChatColor.GREEN + "La monnaie " + args[1] + " a été supprimé!");
					break;
				default:
					break;
				}
			} else
			{
				sender.sendMessage(ChatColor.RED + "La monnaie " + args[1] + " n'existe pas!");
			}
		} else
			sender.sendMessage(ChatColor.RED + "Commande invalide!");
	}

	private void onUpdate(CommandSender sender, String[] args)
	{
		if (args.length == 4)
		{
			Currency currency = Currency.GetByName(args[1]);
			if (currency != null)
			{
				switch (args[2].toUpperCase())
				{
				case "NAME":
					currency.name = args[3];
					break;
				case "SYMBOL":
					currency.symbol = args[3];
					break;
				case "ISDEFAULT":
					currency.isDefault = Boolean.valueOf(args[3]);
					break;
				case "ISTRADABLE":
					currency.tradable = Boolean.valueOf(args[3]);
					break;
				default:
					sender.sendMessage(ChatColor.RED + "Commande invalide!");
					return;
				}

				switch (currency.save())
				{
				case Success:
					sender.sendMessage(ChatColor.GREEN + "Currency updated!");
					break;
				case Failed:
					sender.sendMessage(ChatColor.RED + "Currency update failed!");
					break;
				default:
					break;
				}
			} else
				sender.sendMessage(ChatColor.RED + "La monnaie " + args[1] + " n'existe pas!");
		} else
		{
			sender.sendMessage(ChatColor.RED + "Commande invalide!");
		}
	}

	private void onCreate(CommandSender sender, String[] args)
	{
		if (args.length == 5)
		{
			Currency currency = new Currency(args[1], args[2], Boolean.parseBoolean(args[3]),
					Boolean.parseBoolean(args[4]));
			switch (currency.save())
			{
			case Success:
				sender.sendMessage(ChatColor.GREEN + "Currency created!");
				break;
			case Failed:
				sender.sendMessage(ChatColor.RED + "Currency creation failed!");
				break;
			default:
				break;
			}
		} else
		{
			sender.sendMessage(ChatColor.RED + "Commande invalide!");
		}
	}

}