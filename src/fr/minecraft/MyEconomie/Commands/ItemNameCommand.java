package fr.minecraft.MyEconomie.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import Commands.MinecraftCommand;

public class ItemNameCommand extends MinecraftCommand
{
	static
	{
		COMMAND_NAME = "itemname";
		TAB_COMPLETER = null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (sender instanceof Player)
		{
			Player player = (Player) sender;
			ItemStack item = player.getItemInHand();
			if (item != null)
			{
				player.sendMessage(ChatColor.GOLD + "Nom de l'item: " + item.getType().name());
			} else
				player.sendMessage(ChatColor.RED + "Vous n'avez pas d'item en main!");
		} else
			sender.sendMessage(ChatColor.RED + "Impossible pour un non-joueur!");
		return true;
	}
}
