package fr.minecraft.MyEconomie.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyEconomie.Controllers.AdminShopController;
import fr.minecraft.MyEconomie.Controllers.ShopController;
import fr.minecraft.MyEconomie.Models.Currency;
import fr.minecraft.MyEconomie.TabCompleters.AdminShopTabCompleter;

public class AdminShopCommand extends MinecraftCommand
{
	static
	{
		COMMAND_NAME = "adminShop";
		TAB_COMPLETER = AdminShopTabCompleter.class;
	}

	/**
	 * adminshop [update] [buy | sell | currency] [value]
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (sender instanceof Player)
		{
			Player player = (Player) sender;
			if (args.length == 3)
			{
				switch (args[0].toUpperCase())
				{
				case "UPDATE":
					this.onUpdateArg(player, args);
					break;
				default:
					player.sendMessage(ChatColor.RED + "Commande invalide!");
					break;
				}
			} else
				player.sendMessage(ChatColor.RED + "Commande invalide!");
		} else
			sender.sendMessage(ChatColor.RED + "Commande imposible pour un non-joueur!");
		return true;
	}

	private void onUpdateArg(Player player, String[] args)
	{
		switch (args[1].toUpperCase())
		{
		case "BUY":
			try
			{
				double buy = ShopController.getMoneyValue(args[2]);
				AdminShopController.UpdateBuy(player, buy);
			} catch (NumberFormatException e)
			{
				player.sendMessage(ChatColor.RED + "Le prix est invalide!");
			}
			break;
		case "SELL":
			try
			{
				double sell = ShopController.getMoneyValue(args[2]);
				AdminShopController.UpdateSell(player, sell);
			} catch (NumberFormatException e)
			{
				player.sendMessage(ChatColor.RED + "Le prix est invalide!");
			}
			break;
		case "CURRENCY":
			Currency currency = Currency.GetByNameOrSymbol(args[2]);
			if (currency != null)
				AdminShopController.UpdateCurrency(player, currency);
			else
				player.sendMessage(ChatColor.RED + "Monnaie invalide!");
			break;
		default:
			player.sendMessage(ChatColor.RED + "Commande invalide!");
			break;
		}
	}
}
