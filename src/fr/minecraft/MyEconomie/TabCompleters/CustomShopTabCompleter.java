package fr.minecraft.MyEconomie.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftTabCompleter;
import fr.minecraft.MyEconomie.Controllers.CustomShopController;
import fr.minecraft.MyEconomie.Models.CustomShop;

public class CustomShopTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		ArrayList<String> res = new ArrayList<String>();

		switch (args.length)
		{
		case 1:
			res.add("ADD");
			res.add("REMOVE");
			res.add("UPDATE");
			break;
		case 2:
			switch (args[0].toUpperCase())
			{
			case "ADD":
			case "REMOVE":
				res = this.onAddRemoveArg(args);
				break;
			case "UPDATE":
				res = this.onUpdateArg(args);
				break;
			default:
				res = null;
			}
			break;
		case 3:
			res = this.onArgs3(args, sender);
			break;
		}
		return res;
	}

	private ArrayList<String> onArgs3(String[] args, CommandSender sender)
	{
		ArrayList<String> res = new ArrayList<String>();
		if (sender instanceof Player)
		{
			Player player = (Player) sender;
			if (args[0].equalsIgnoreCase("REMOVE"))
			{
				CustomShop shop = CustomShopController.GetShop(player);
				if (shop != null)
				{
					switch (args[1].toUpperCase())
					{
					case "REQUIRE":
						res = shop.requires;
						break;
					case "FORBIDDEN":
						res = shop.forbiddens;
						break;
					case "COMMAND":
						res = shop.commands;
						break;
					}
				}
			} else if (args[2].isEmpty())
				res.add("[value]");
		}
		return res;
	}

	private ArrayList<String> onUpdateArg(String[] args)
	{
		ArrayList<String> res = new ArrayList<String>();
		res.add("DESCRIPTION");
		res.add("BUY");
		res.add("CURRENCY");
		res.add("BREAKAFTERBUYED");
		return res;
	}

	private ArrayList<String> onAddRemoveArg(String[] args)
	{
		ArrayList<String> res = new ArrayList<String>();
		res.add("REQUIRE");
		res.add("FORBIDDEN");
		res.add("COMMAND");
		return res;
	}
}
