package fr.minecraft.MyEconomie.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftTabCompleter;
import fr.minecraft.MyEconomie.Models.Currency;

public class AdminShopTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		ArrayList<String> res = new ArrayList<String>();

		switch (args.length)
		{
		case 1:
			res.add("UPDATE");
			break;
		case 2:
			switch (args[0].toUpperCase())
			{
			case "UPDATE":
				res.add("BUY");
				res.add("SELL");
				res.add("CURRENCY");
				break;
			default:
				res = null;
			}
			break;
		case 3:
			switch (args[1].toUpperCase())
			{
			case "UPDATE":
				res = this.onUpdateArg3(args);
				break;
			default:
				res = null;
			}
			break;
		}
		return res;
	}

	private ArrayList<String> onUpdateArg3(String[] args)
	{
		ArrayList<String> res = new ArrayList<String>();
		switch (args[2].toUpperCase())
		{
		case "SELL":
		case "BUY":
			res.add("[valeur]");
			break;
		case "CURRENCY":
			for (Currency currency : Currency.GetAll())
			{
				res.add(currency.name);
			}
			break;
		default:
			res = null;
		}
		return res;
	}
}
