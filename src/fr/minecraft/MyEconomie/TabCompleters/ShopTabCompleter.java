package fr.minecraft.MyEconomie.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftTabCompleter;

public class ShopTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res = null;

		switch (args.length)
		{
		case 1:
			res = new ArrayList<String>();
			res.add("take");
			res.add("store");
			res.add("update");
			break;
		case 2:
			res = this.onArg2(args);
			break;
		case 3:
			res = this.onArg3(args);
			break;
		}
		return res;
	}

	private List<String> onArg2(String[] args)
	{
		List<String> res = new ArrayList<String>();
		switch (args[0].toUpperCase())
		{
		case "UPDATE":
			res.add("quantity");
			res.add("buy");
			res.add("sell");
			break;
		default:
			if (args[1].isEmpty())
				res.add("[amount]");
			else
				res = null;
			break;
		}
		return res;
	}

	private List<String> onArg3(String[] args)
	{
		List<String> res = new ArrayList<String>();
		if (args[2].isEmpty())
			res.add("[amount]");
		else
			res = null;
		return res;
	}
}
