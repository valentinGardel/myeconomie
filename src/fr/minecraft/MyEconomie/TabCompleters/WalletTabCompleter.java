package fr.minecraft.MyEconomie.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftTabCompleter;
import fr.minecraft.MyEconomie.Models.Currency;

public class WalletTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res = new ArrayList<String>();

		switch (args.length)
		{
		case 2:
			List<Currency> currencies = Currency.GetAll();
			for (Currency currency : currencies)
			{
				if (currency.name.toUpperCase().startsWith(args[1].toUpperCase()))
				{
					res.add(currency.name);
				}
			}
			break;
		case 3:
			if (sender.hasPermission("MyEconomie.setWallet"))
			{
				res.add("set");
				res.add("add");
				res.add("remove");
			}
			break;
		case 4:
			if (sender.hasPermission("MyEconomie.setWallet"))
			{
				if (args[3].isEmpty())
					res.add("[amount]");
			}
			break;
		default:
			res = null;
			break;
		}
		return res;
	}

}
