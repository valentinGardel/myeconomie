package fr.minecraft.MyEconomie.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftTabCompleter;
import fr.minecraft.MyEconomie.Models.Currency;

public class WalletBoardTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res = new ArrayList<String>();

		switch (args.length)
		{
		case 1:
			List<Currency> currencies = Currency.GetAll();
			for (Currency currency : currencies)
			{
				if (currency.name.toUpperCase().startsWith(args[0].toUpperCase()))
				{
					res.add(currency.name);
				}
			}
			break;
		case 2:
			if (args[1].isEmpty())
				res.add("[<page>]");
			break;
		default:
			res = null;
			break;
		}
		return res;
	}

}
