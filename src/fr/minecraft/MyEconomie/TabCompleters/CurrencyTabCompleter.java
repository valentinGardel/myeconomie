package fr.minecraft.MyEconomie.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftTabCompleter;
import fr.minecraft.MyEconomie.Models.Currency;

public class CurrencyTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res = null;

		if (args.length > 1)
		{
			switch (args[0].toUpperCase())
			{
			case "CREATE":
				res = this.onCreate(args);
				break;
			case "DELETE":
				res = this.onDelete(args);
				break;
			case "UPDATE":
				res = this.onUpdate(args);
				break;
			default:
				res = null;
				break;
			}
		} else
		{
			res = this.options(args);
		}
		return res;
	}

	private List<String> onUpdate(String[] args)
	{
		List<String> res = new ArrayList<String>();
		switch (args.length)
		{
		case 2:
			for (Currency currency : Currency.GetAll())
			{
				if (currency.name.toUpperCase().startsWith(args[1].toUpperCase()))
					res.add(currency.name);
			}
			break;
		case 3:
			res.add("name");
			res.add("symbol");
			res.add("isTradable");
			res.add("isDefault");
			break;
		case 4:
			switch (args[2].toUpperCase())
			{
			case "NAME":
				if (args[3].isEmpty())
					res.add("[name]");
				break;
			case "SYMBOL":
				if (args[3].isEmpty())
					res.add("[symbol]");
				break;
			case "ISTRADABLE":
				res.add("TRUE");
				res.add("FALSE");
				break;
			case "ISDEFAULT":
				res.add("TRUE");
				res.add("FALSE");
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		return res;
	}

	private List<String> onDelete(String[] args)
	{
		List<String> res = new ArrayList<String>();
		for (Currency currency : Currency.GetAll())
		{
			if (currency.name.toUpperCase().startsWith(args[1].toUpperCase()))
				res.add(currency.name);
		}
		return res;
	}

	private List<String> options(String[] args)
	{
		List<String> res = new ArrayList<String>();
		res.add("create");
		res.add("list");
		res.add("delete");
		res.add("update");
		return res;
	}

	private List<String> onCreate(String[] args)
	{
		List<String> res = new ArrayList<String>();
		switch (args.length - 1)
		{
		case 1:
			if (args[1].isEmpty())
				res.add("[name]");
			break;
		case 2:
			if (args[2].isEmpty())
				res.add("[symbol]");
			break;
		case 3:
			if (args[3].isEmpty())
				res.add("[isDefault]");
			else
			{
				if ("TRUE".startsWith(args[3].toUpperCase()))
					res.add("TRUE");
				else
					res.add("FALSE");
			}
			break;
		case 4:
			if (args[4].isEmpty())
				res.add("[isTradable]");
			else
			{
				if ("TRUE".startsWith(args[4].toUpperCase()))
					res.add("TRUE");
				else
					res.add("FALSE");
			}
			break;
		default:
			res = null;
			break;
		}
		return res;
	}
}
