package fr.minecraft.MyEconomie.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftTabCompleter;

public class ShopSearchTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		ArrayList<String> res = null;
		if (args.length == 1)
		{
			res = new ArrayList<String>();
			for (Material m : Material.values())
			{
				if (m.name().toUpperCase().startsWith(args[0].toUpperCase()))
					res.add(m.name());
			}
		}
		return res;
	}
}
