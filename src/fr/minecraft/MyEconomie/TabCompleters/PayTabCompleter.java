package fr.minecraft.MyEconomie.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftTabCompleter;
import fr.minecraft.MyEconomie.Models.Currency;

public class PayTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res = new ArrayList<String>();

		switch (args.length)
		{
		case 1:
			res = null;
			/*
			 * if(args[0].isEmpty()) res.add("[player]"); else res=null;
			 */
			break;
		case 2:
			if (args[1].isEmpty())
				res.add("[amount]");
			else
				res = null;
			break;
		case 3:
			List<Currency> currencies = Currency.GetAll();
			for (Currency currency : currencies)
			{
				if (currency.name.toUpperCase().startsWith(args[2].toUpperCase()))
				{
					res.add(currency.name);
				}
			}
			break;
		default:
			res = null;
			break;
		}
		return res;
	}
}
