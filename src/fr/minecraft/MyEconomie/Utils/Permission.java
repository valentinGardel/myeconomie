package fr.minecraft.MyEconomie.Utils;

public class Permission
{
	public static final String

	SET_WALLET = "MyEconomie.setWallet", OTHERS_WALLET = "MyEconomie.othersWallet", CAN_CREATE_SHOP = "MyEconomie.shop",
			CAN_CREATE_ADMIN_SHOP = "MyEconomie.createAdminShop",
			CAN_CREATE_CUSTOM_SHOP = "MyEconomie.createCustomShop", BYPASS_SHOP = "MyEconomie.bypassShop";
}
