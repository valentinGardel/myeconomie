package fr.minecraft.MyEconomie.Utils;

public enum Code
{
	Success, Failed, NotInDatabase, ParamInvalid, MaxAmountReached, NotEnoughFound, NotEnoughItem, NotAvailable
}
