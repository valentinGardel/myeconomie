package fr.minecraft.MyEconomie.Listeners;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerSignOpenEvent;

import Commons.MinecraftListener;
import fr.minecraft.MyEconomie.Commands.CustomShopSelectorCommand;
import fr.minecraft.MyEconomie.Controllers.AdminShopController;
import fr.minecraft.MyEconomie.Controllers.CustomShopController;
import fr.minecraft.MyEconomie.Controllers.ShopController;
import fr.minecraft.MyEconomie.Models.CustomShop;
import fr.minecraft.MyEconomie.Models.ShopSign;
import fr.minecraft.MyEconomie.Models.Wallet;
import fr.minecraft.MyEconomie.Utils.Permission;

public class SignListener extends MinecraftListener
{
	private static final BlockFace[] FACE_TO_CHECK =
	{ BlockFace.EAST, BlockFace.NORTH, BlockFace.WEST, BlockFace.SOUTH };
	private static final Material[] BLOCK_PISTON_IGNORE =
	{ Material.CHEST, Material.BEDROCK, Material.BARRIER };

	/**
	 * Event to prevent breaking block who support shop
	 */
	@EventHandler
	public void onBlockBreakEvent(BlockBreakEvent e)
	{
		if (!e.isCancelled())
		{
			e.setCancelled(check(e.getBlock()));
			e.isCancelled();
		}
	}

	/**
	 * Check if a block support a shop sign
	 * 
	 * @return true if the block support a shop sign
	 */
	public boolean check(Block b)
	{
		if (this.blockIsPistonIgnored(b.getType()))
		{
			for (BlockFace f : FACE_TO_CHECK)
			{
				Block sign = b.getRelative(f);
				if (this.isShopSign(sign))
				{
					BlockData data = sign.getBlockData();
					if (data instanceof Directional)
					{
						Directional directional = (Directional) data;
						if (directional.getFacing() == f)
							return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Check if a material is piston ignored
	 * 
	 * @return true if the material is piston ignored
	 */
	private boolean blockIsPistonIgnored(Material material)
	{
		/*
		 * Bukkit.broadcastMessage(b.getType().name()+", "+b.getPistonMoveReaction().
		 * name()); return b.getPistonMoveReaction() == PistonMoveReaction.BLOCK; /
		 */
		for (Material m : BLOCK_PISTON_IGNORE)
		{
			if (m == material)
				return true;
		}
		return false;
		// */
	}

	/**
	 * Check if player Interact with a shop (buy, sell or update)
	 */
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onSignClic(PlayerInteractEvent event)
	{
		if (!event.isCancelled() && event.getClickedBlock() != null)
		{
			if (this.isNormalShopSign(event.getClickedBlock()))
			{
				ShopSign sign = ShopSign.GetByLocation(event.getClickedBlock().getLocation());
				if (sign != null)
				{
					/* select */
					if (event.getPlayer().getGameMode() == GameMode.CREATIVE)
					{
						if (event.getPlayer().hasPermission(Permission.CAN_CREATE_ADMIN_SHOP)
								&& event.getAction() == Action.RIGHT_CLICK_BLOCK
//								&& event.getPlayer().hasPermission(Permission.CAN_CREATE_CUSTOM_SHOP)
						)
							AdminShopController.SelectSign(event.getPlayer(), event.getClickedBlock().getLocation());
					}
					/* buy or sell */
					else if (!event.getPlayer().equals(sign.owner))
					{
						int amount = 1;
						/* The player wants to buy */
						if (event.getAction() == Action.RIGHT_CLICK_BLOCK)
						{
							if (event.getPlayer().isSneaking())
							{
								amount = sign.item.getMaxStackSize();
							}
							switch (sign.buy(Wallet.GetByPlayer(event.getPlayer()), amount, event.getPlayer()))
							{
							case Failed:
								event.getPlayer().sendMessage(ChatColor.RED + "Failed!");
								break;
							case NotAvailable:
								event.getPlayer().sendMessage(ChatColor.RED + "Ce shop ne permet pas de d'acheter!");
								break;
							case NotEnoughFound:
								event.getPlayer().sendMessage(ChatColor.RED + "Vous n'avez pas assez de fond!");
								break;
							case NotEnoughItem:
								event.getPlayer().sendMessage(ChatColor.RED + "Le shop n'a plus d'item!");
								break;
							case Success:
								event.getPlayer().sendMessage(ChatColor.GREEN + "Vous avez acheté " + amount + " "
										+ sign.item.name() + "(s)!");
								break;
							default:
								break;
							}
						}
						/* The player wants to sell */
						else if (event.getAction() == Action.LEFT_CLICK_BLOCK)
						{
							if (event.getPlayer().isSneaking())
							{
								amount = sign.item.getMaxStackSize();
								// Si le joueur n'a pas un stack entier, il vends ce qu'il lui reste
								int hasAmount = ShopSign.PlayerHas(event.getPlayer(), sign.item);
								amount = amount > hasAmount && hasAmount > 0 ? hasAmount : amount;
							}
							if (event.getPlayer().getGameMode() != GameMode.CREATIVE)
							{
								switch (sign.sell(Wallet.GetByPlayer(event.getPlayer()), amount, event.getPlayer()))
								{
								case Failed:
									event.getPlayer().sendMessage(ChatColor.RED + "Failed!");
									break;
								case MaxAmountReached:
									event.getPlayer().sendMessage(ChatColor.RED + "La quantité maximal est atteinte!");
									break;
								case NotAvailable:
									event.getPlayer().sendMessage(ChatColor.RED + "Ce shop ne permet pas de vendre!");
									break;
								case NotEnoughFound:
									event.getPlayer().sendMessage(ChatColor.RED + "Le vendeur n'a pas assez de fond!");
									break;
								case NotEnoughItem:
									event.getPlayer().sendMessage(ChatColor.RED + "Vous n'avez pas assez d'item!");
									break;
								case Success:
									event.getPlayer().sendMessage(ChatColor.GREEN + "Vous avez vendu " + amount + " "
											+ sign.item.name() + "(s)!");
									break;
								default:
									break;
								}
							}
						}
					}
					/* selecting sign */
					else if (sign.owner != null && event.getAction() == Action.RIGHT_CLICK_BLOCK)
						ShopController.SelectSign(event.getPlayer(), event.getClickedBlock().getLocation());
				}
			} else if (this.isCustomShopSign(event.getClickedBlock()))
			{
				CustomShop sign = CustomShop.GetByLocation(event.getClickedBlock().getLocation());
				if (sign != null)
				{
					// toggle sign
					if (event.getItem() != null && event.getItem().getItemMeta().getDisplayName()
							.equals(CustomShopSelectorCommand.SELECTOR_NAME))
					{
						if (event.getPlayer().hasPermission(Permission.CAN_CREATE_CUSTOM_SHOP))
						{
							Sign s = (Sign) sign.location.getBlock().getState();

							if (s.getLine(0).equalsIgnoreCase(CustomShop.FIRST_LINE))
								s.setLine(0, CustomShop.FIRST_LINE_DISABLE);
							else
								s.setLine(0, CustomShop.FIRST_LINE);

							s.update();
							CustomShopController.SelectSign(event.getPlayer(), event.getClickedBlock().getLocation());
						} else
							event.getPlayer().sendMessage(ChatColor.RED + "Vous n'avez pas la permission de faire ca!");
					} else
					{
						if (event.getPlayer().getGameMode() != GameMode.CREATIVE)
						{
							// if player edit shop
							if (event.getAction() == Action.LEFT_CLICK_BLOCK)
							{
								if (event.getPlayer().hasPermission(Permission.CAN_CREATE_CUSTOM_SHOP))
								{
									CustomShopController.SelectSign(event.getPlayer(),
											event.getClickedBlock().getLocation());
								}
							}
							// if player buy
							else if (event.getAction() == Action.RIGHT_CLICK_BLOCK)
							{
								if (((Sign) sign.location.getBlock().getState()).getLine(0)
										.equalsIgnoreCase(CustomShop.FIRST_LINE))
									CustomShopController.Buy(event.getPlayer(), event.getClickedBlock().getLocation());
								else
									event.getPlayer().sendMessage(ChatColor.RED + "Custom Shop désactivé!");
							}
						} else if (event.getPlayer().hasPermission(Permission.CAN_CREATE_CUSTOM_SHOP))
							CustomShopController.SelectSign(event.getPlayer(), event.getClickedBlock().getLocation());
					}
				}
			}
		}
	}

	/**
	 * Check if player break a shop sign
	 */
	@EventHandler
	public void onDestroySign(BlockBreakEvent event)
	{
		if (!event.isCancelled())
		{
			if (this.isNormalShopSign(event.getBlock()))
			{
				ShopSign sign = ShopSign.GetByLocation(event.getBlock().getLocation());
				if (sign != null)
				{
					if (sign.owner != null)
					{
						if (sign.itemQuantity > 0)
						{
							event.getPlayer().sendMessage(ChatColor.RED
									+ "Impossible de supprimer un panneau qui contient encore des items!");
							event.setCancelled(true);
						} else if (event.getPlayer().equals(sign.owner)
								|| event.getPlayer().hasPermission(Permission.BYPASS_SHOP))
						{
							sign.delete();
							event.getPlayer().sendMessage(ChatColor.GREEN + "Shop sign supprimé!");
						} else
						{
							event.getPlayer().sendMessage(ChatColor.RED
									+ "Vous n'avez pas le droit de supprimer un shop qui n'est pas le votre!");
							event.setCancelled(true);
						}
					} else
					{
						if (event.getPlayer().hasPermission(Permission.CAN_CREATE_ADMIN_SHOP))
						{
							sign.delete();
							event.getPlayer().sendMessage(ChatColor.GREEN + "Shop sign supprimé!");
						} else
						{
							event.getPlayer().sendMessage(
									ChatColor.RED + "Vous n'avez pas le droit de supprimer un shop admin!");
							event.setCancelled(true);
						}
					}
				}
			} else if (this.isCustomShopSign(event.getBlock()))
			{
				CustomShop sign = CustomShop.GetByLocation(event.getBlock().getLocation());
				if (sign != null)
				{
					if (event.getPlayer().hasPermission(Permission.CAN_CREATE_CUSTOM_SHOP))
					{
						sign.delete();
						event.getPlayer().sendMessage(ChatColor.GREEN + "CustomShop sign supprimé!");
					} else
					{
						event.getPlayer()
								.sendMessage(ChatColor.RED + "Vous n'avez pas le droit de supprimer un custom shop!");
						event.setCancelled(true);
					}
				}
			}
		}
	}

	/**
	 * Check if the block is a shopsign (Already created)
	 */
	private boolean isShopSign(Block block)
	{
		if (block.getType().name().toUpperCase().endsWith("_WALL_SIGN"))
		{
			return this.stringIsShopFirstLine(ShopSign.getSignLine((Sign) block.getState(), 0));
		}
		return false;
	}

	/**
	 * Check if the block is a shopsign (During creation)
	 */
	private boolean isShopSign(Block block, String firstLine)
	{
		// Bukkit.broadcastMessage(block.getType().name()+", "+firstLine);
		if (block.getType().name().toUpperCase().endsWith("_WALL_SIGN"))
		{
			return this.stringIsShopFirstLine(firstLine);
		}
		return false;
	}

	/**
	 * Check if the string is the first line of a shop
	 */
	private boolean stringIsShopFirstLine(String firstLine)
	{
		return firstLine.equalsIgnoreCase(ShopSign.FIRST_LINE_ADMIN)
				|| firstLine.equalsIgnoreCase(ShopSign.FIRST_LINE_USER)
				|| firstLine.equalsIgnoreCase(CustomShop.FIRST_LINE);
	}

	private boolean isNormalShopSign(Block clickedBlock)
	{
		if (clickedBlock.getType().name().endsWith("_WALL_SIGN"))
		{
			String line = ShopSign.getSignLine((Sign) clickedBlock.getState(), 0);
			return line.equalsIgnoreCase(ShopSign.FIRST_LINE_ADMIN) || line.equalsIgnoreCase(ShopSign.FIRST_LINE_USER);
		}
		return false;
	}

	private boolean isCustomShopSign(Block clickedBlock)
	{
		if (clickedBlock.getType().name().endsWith("_WALL_SIGN"))
		{
			String line = ShopSign.getSignLine((Sign) clickedBlock.getState(), 0);
			return line.equalsIgnoreCase(CustomShop.FIRST_LINE) || line.equalsIgnoreCase(CustomShop.FIRST_LINE_DISABLE);
		}
		return false;
	}

	/**
	 * Event handler who detect when a shop sign is edited
	 */
	@EventHandler
	public void onPlayerSignOpenEvent(PlayerSignOpenEvent event)
	{
		if (!event.isCancelled())
		{
			if (this.isShopSign(event.getSign().getBlock(), ShopSign.getSignLine(event.getSign(), 0)))
			{
				event.setCancelled(true);
			}
		}
	}

	/**
	 * Event handler who detect when a shop sign is created
	 */
	@EventHandler
	public void onSignChangeEvent(SignChangeEvent event)
	{
		if (!event.isCancelled())
		{
			// event.getPlayer().sendMessage("pas cancelled,
			// "+event.getBlock().getType().name());
			// if sign start with a shop line and is on wall
			if (this.isShopSign(event.getBlock(), event.getLine(0)))
			{
				// event.getPlayer().sendMessage("est shop");
				Block block = event.getBlock()
						.getRelative(((Directional) event.getBlock().getBlockData()).getFacing().getOppositeFace());
				// if sign is placed on a block which ignore pistons
				// event.getPlayer().sendMessage(block.getType().name());
				if (this.blockIsPistonIgnored(block.getType()))
				{
					// event.getPlayer().sendMessage("est sur un bon block");
					if (event.getLine(0).equalsIgnoreCase(ShopSign.FIRST_LINE_USER))
					{
						if (event.getPlayer().hasPermission(Permission.CAN_CREATE_SHOP))
						{
							ShopController.ProcessShopSignCreation(event, false);
						} else
						{
							event.getPlayer()
									.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de créer de shop!");
							event.setCancelled(true);
						}
					} else if (event.getLine(0).equalsIgnoreCase(ShopSign.FIRST_LINE_ADMIN))
					{
						if (event.getPlayer().hasPermission(Permission.CAN_CREATE_ADMIN_SHOP))
						{
							ShopController.ProcessShopSignCreation(event, true);
						} else
						{
							event.getPlayer().sendMessage(
									ChatColor.RED + "Vous n'avez pas la permission de créer de shop admin!");
							event.setCancelled(true);
						}
					} else if (event.getLine(0).equalsIgnoreCase(CustomShop.FIRST_LINE))
					{
						if (event.getPlayer().hasPermission(Permission.CAN_CREATE_CUSTOM_SHOP))
						{
							CustomShopController.ProcessShopSignCreation(event);
						} else
						{
							event.getPlayer().sendMessage(
									ChatColor.RED + "Vous n'avez pas la permission de créer de shop custom!");
							event.setCancelled(true);
						}
					}
				} else
				{
					event.getPlayer().sendMessage(ChatColor.RED
							+ "Impossible de créer un shop sur ce type de bloc, vous devez le poser sur un bloc fixe!");
					event.setCancelled(true);
				}
			}
		}
	}
}
