package fr.minecraft.MyEconomie;

import Commons.MinecraftPlugin;
import fr.minecraft.MyEconomie.Commands.*;
import fr.minecraft.MyEconomie.Listeners.*;
import fr.minecraft.MyEconomie.Models.*;

@SuppressWarnings("unchecked")
public class Main extends MinecraftPlugin
{
	static
	{
		PLUGIN_NAME = "MyEconomie";
		LOG_TOGGLE = false;
		COMMANDS = new Class[]
		{ CurrencyCommand.class, PayCommand.class, WalletCommand.class, WalletBoardCommand.class, ShopCommand.class,
				CustomShopCommand.class, AdminShopCommand.class, ShopSearchCommand.class, ItemNameCommand.class,
				CustomShopSelectorCommand.class, };
		LISTENERS = new Class[]
		{ SignListener.class };
		CONFIG = Config.class;
	}

	@Override
	protected void initDatabase() throws Exception
	{
		Currency.CreateTable();
		ShopSign.CreateTable();
		CustomShop.CreateTable();
		Wallet.CreateTable();
	}
}
